#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/fb.h>
#include <stdlib.h>

#include  <stdio.h>
#include  <time.h>
#include <poll.h>

typedef long long  int64_t;

#define TIME_DECLARE(prefix)   \
  struct timespec prefix ## _ts_beg, prefix ## _ts_end; long prefix ## _diff;
#define TIME_BEGIN(prefix)   \
  clock_gettime( CLOCK_MONOTONIC, &prefix ##_ts_beg );
#define TIME_END(prefix,limit,msg)                              \
  clock_gettime( CLOCK_MONOTONIC, &prefix ##_ts_end );  \
  if ( (prefix##_diff = \
        ((prefix##_ts_end.tv_sec  - prefix##_ts_beg.tv_sec )<<10) +      \
        ((prefix##_ts_end.tv_nsec - prefix##_ts_beg.tv_nsec)>>20)) > limit ) msg;

//  msg

#define LOG_MAX 80
#define RK_FBIOSET_VSYNC_ENABLE 0x4629

int main(int argc, const char*argv[] )
{
  int err;
  char  buff[1024];
  const char* msg=0;
  int64_t  log[LOG_MAX];
  int cnt_real = 0;
  int log_cnt=0;
  TIME_DECLARE(t);


  // enable vsync
  int fdFB = open("/dev/graphics/fb0", O_RDWR, 0);
  int enable = 1;
  err = ioctl(fdFB, RK_FBIOSET_VSYNC_ENABLE, &enable);
  printf( "vsync enable rc=%d(%d)\n", err, enable);



  
  int fd = open("/sys/class/graphics/fb0/vsync", O_RDONLY, 0);
  msg="open"; if (fd <=0) goto err;

  err = read(fd, buff, sizeof(buff));
  msg="read"; if (err < 0) goto err;

  struct pollfd fds[1];
  fds[0].fd = fd;
  fds[0].events = POLLPRI; 

  TIME_BEGIN(t);
  for (int cnt=72 ; --cnt >= 0 ; ) {
    err = poll(fds, 1, -1);
    if (err > 0 && ( fds[0].revents & POLLPRI )) { 

      err = lseek(fd, 0, SEEK_SET);
      msg="lseek"; if (err < 0) goto err;
      err = read(fd, buff, sizeof(buff));
      msg="read2"; if (err < 0) goto err;

      if ( err > 0 ) {
        cnt_real ++;
      
        int64_t curr  = strtoull(buff, NULL, 0);
        if (log_cnt < LOG_MAX) {
          log[log_cnt] = curr;
          log_cnt ++;
        }
      }
    }
  }
  TIME_END(t,1,printf("elapsed time=%ld, cnt_real=%d\n", t_diff, cnt_real));

  for (int i = 0 ; i ++ < log_cnt-1 ; ) {
    printf("i=%d time=%lld diff=%lld\n", i, log[i], log[i+1] - log[i]);
  }

  msg=0;
 err:
  const char* msg2 = msg? "error" : (msg="", "OK");
  printf("%s(%s)\n", msg2, msg );
  if ( fd > 0 )
    close(fd);
  return 0;
}

/* execution output

vsync enable rc=0(1)
elapsed time=1017, cnt_real=72
i=1 time=55069515901 diff=13831125
i=2 time=55083347026 diff=13833750
i=3 time=55097180776 diff=13839291
i=4 time=55111020067 diff=13833750
i=5 time=55124853817 diff=13834917
i=6 time=55138688734 diff=13834333
i=7 time=55152523067 diff=13832292

...
i=66 time=55968718693 diff=13834041
i=67 time=55982552734 diff=13833750
i=68 time=55996386484 diff=13865250
i=69 time=56010251734 diff=13802834
i=70 time=56024054568 diff=13833166
i=71 time=56037887734 diff=-5265644507988119067
OK()

*/
