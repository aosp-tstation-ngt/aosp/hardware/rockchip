#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/fb.h>
#include <stdlib.h>
#include <sys/mman.h>

#include  <stdio.h>
#include  <time.h>
#include  <assert.h>

#define ALOG(fmt,...) printf(fmt "\n",__VA_ARGS__)
#define ALOG0(fmt) printf(fmt "\n")

#define RK_FBIOSET_CONFIG_DONE         0x4628
#define RK_FBIOGET_OVERLAY_STA   			0X4619
#define RK_FBIOSET_OVERLAY_STA    			0x5018


//----------------------------------------------------------
// 
//----------------------------------------------------------
#define RK_WIN_MAX_AREA		    4
#define RK_MAX_BUF_NUM       	11
#define RK30_MAX_LAYER_SUPPORT	5
#define VOP_WIN_NUM       2

struct rk_fb_area_par {
	uint8_t  data_format;        /*layer data fmt*/
	short ion_fd;
	unsigned int phy_addr;
	short acq_fence_fd;
	uint16_t  x_offset;
	uint16_t  y_offset;
	uint16_t xpos;		/*start point in panel  --->LCDC_WINx_DSP_ST*/
	uint16_t ypos;
	uint16_t xsize;		/* display window width/height  -->LCDC_WINx_DSP_INFO*/
	uint16_t ysize;
	uint16_t xact;		/*origin display window size -->LCDC_WINx_ACT_INFO*/
	uint16_t yact;
	uint16_t xvir;		/*virtual width/height     -->LCDC_WINx_VIR*/
	uint16_t yvir;
	uint8_t  fbdc_en;
	uint8_t  fbdc_cor_en;
	uint8_t  fbdc_data_format;
	uint16_t data_space;
	uint32_t reserved0;
};

struct rk_fb_win_par {
	uint8_t  win_id;
	uint8_t  z_order;		/*win sel layer*/
	uint8_t  alpha_mode;
	uint16_t g_alpha_val;
	uint8_t  mirror_en;
	struct rk_fb_area_par area_par[RK_WIN_MAX_AREA];
	uint32_t reserved0;
};

struct rk_lcdc_post_cfg{
	uint32_t xpos;
	uint32_t ypos;
	uint32_t xsize;
	uint32_t ysize;
};

struct rk_fb_win_cfg_data {
	uint8_t  wait_fs;
	short ret_fence_fd;
	short rel_fence_fd[RK_MAX_BUF_NUM];
	struct  rk_fb_win_par win_par[RK30_MAX_LAYER_SUPPORT];
	struct  rk_lcdc_post_cfg post_cfg;
};

//----------------------------------------------------------
// ion
//----------------------------------------------------------
typedef int ion_user_handle_t;
extern "C" {
int ion_open();
int ion_close(int fd);
int ion_alloc(int fd, size_t len, size_t align, unsigned int heap_mask,
              unsigned int flags, ion_user_handle_t *handle);
int ion_free(int fd, ion_user_handle_t handle);
int ion_map(int fd, ion_user_handle_t handle, size_t length, int prot,
            int flags, off_t offset, unsigned char **ptr, int *map_fd);
int ion_share(int fd, ion_user_handle_t handle, int *share_fd);
int ion_alloc_fd(int fd, size_t len, size_t align, unsigned int heap_mask,
                 unsigned int flags, int *handle_fd);
int ion_import(int fd, int share_fd, ion_user_handle_t *handle);
 int ion_sync_fd(int fd, int handle_fd);
int ion_get_phys(int fd, ion_user_handle_t handle, unsigned long *phys);
};


//----------------------------------------------------------
// working buffer info
//----------------------------------------------------------
#define NUM_BUFFER    2
//#define NUM_PARAM_A_BUFFER 4  // alpha, pixel_fomat, alpha_mode, g_alpha_val;
//char const* strParam[NUM_PARAM_A_BUFFER] = { "alpha", "pix_fmt", "alpha_mode", "g_alpha_val" };
#define NUM_PARAM_A_BUFFER 3  // alpha, pixel_fomat, black
char const* strParamBuff[NUM_PARAM_A_BUFFER] = { "alpha", "pix_fmt", "black" };

#define NUM_PARAM_COMMON  1  // swap_win01
char const* strParamCommon[NUM_PARAM_COMMON] = { "swap_win01" };

#define NUM_PARAM     (NUM_PARAM_A_BUFFER * NUM_BUFFER + NUM_PARAM_COMMON)

/*
  |  blue  |  black |  blue  |  black |       -- buffer0
      |  red   |  black |  red   |  black |     -- buffer1
 */

#define BUFF0_WIDTH   256
#define BUFF0_HEIGHT  512 //256
#define BUFF0_COLOR   0x00ff0000  // blue

#define BUFF1_WIDTH   256
#define BUFF1_HEIGHT  512
#define BUFF1_COLOR   0x000000ff  // red
//#define BUFF1_COLOR   0x00000000  // black


#define RK_FBIOSET_CONFIG_DONE         0x4628
#define PIXEL_SIZE_IN_BYTES 4
//----------------------------------------------------------
// utility
//----------------------------------------------------------

//void  clear( void* vaddr0, void* vaddr1 )
void  clear( void* vaddr, int size_in_byte, uint32_t color, int alpha )
{
    uint32_t const    argb = ( color & 0xffffff ) | ( (alpha&0xff) << 24 );
    int const         size_in_pixel = size_in_byte / 4;

    // vaddr <--
    memset( vaddr, 0, size_in_byte );
    uint32_t*   pBuff = (uint32_t*)vaddr;
    for ( int i = 0 ; i < size_in_pixel / 4 ; i ++ )
        pBuff[i] = argb;
    for ( int i = size_in_pixel / 2 ; i < size_in_pixel / 4 * 3 ; i ++ )
        pBuff[i] = argb;

}

//----------------------------------------------------------
// main
//----------------------------------------------------------
#define FBIOGET_DMABUF  0x5003

int main(int argc, const char*argv[] )
{
    int       rc = 0;
    int       errLine = 0;
    int       paramBuff[NUM_BUFFER][NUM_PARAM_A_BUFFER];
    int       paramCommon[NUM_PARAM_COMMON];

    int       fdFB[2] = {0,0};
    int const map_mask = PROT_READ | PROT_WRITE;
    // working buffers(0 and 1)
		int       ion_client=0;
		int       ion_hnd[NUM_BUFFER]={0,0}, shareFd[NUM_BUFFER]={0,0};
    void*     vaddr[NUM_BUFFER];//, *vaddr1;
    int const bufferSize[NUM_BUFFER] = {BUFF0_WIDTH * BUFF0_HEIGHT * PIXEL_SIZE_IN_BYTES,
                         BUFF1_WIDTH * BUFF1_HEIGHT * PIXEL_SIZE_IN_BYTES};
    uint32_t const  color[NUM_BUFFER] = { BUFF0_COLOR, BUFF1_COLOR };

    // param[] <-- cmd line
    if ( argc != NUM_PARAM + 1 ) {
        printf("usage : %s  ", argv[0] );
        for ( int b = 0 ; b < NUM_BUFFER ; b ++ )
        for ( int p = 0 ; p < NUM_PARAM_A_BUFFER ; p ++ )
            printf( "%s%d ", strParamBuff[p], b );
        for ( int p = 0 ; p < NUM_PARAM_COMMON; p ++ )
            printf( " %s ", strParamCommon[p] );
        printf("\n  %d parameters are needed\n", NUM_PARAM );
        errLine = __LINE__; goto ret;
    }
    //printf( "  -->" );
    {
        int   i = 0;
        for ( int b = 0 ; b < NUM_BUFFER ; b ++ ) 
            for ( int p = 0 ; p < NUM_PARAM_A_BUFFER ; p ++ ) 
                paramBuff[b][p] = atoi( argv[++i] );
        for ( int p = 0 ; p < NUM_PARAM_COMMON ; p ++ ) 
            paramCommon[p] = atoi( argv[++i] );
    }
    //printf(" : index=%d\n", index);
    

    // fdFB[] <-- 
    for ( int i = 0 ; i < 2 ; i ++ ) {
        char const*const  strDev[2] = { "/dev/graphics/fb0", "/dev/graphics/fb1" };
        if ( (fdFB[i] = open(strDev[i], O_RDWR, 0) ) <= 0 ) { errLine = __LINE__; goto ret; }
    }


    // working buffer
		if ( ( ion_client = ion_open() ) <= 0 ) { errLine = __LINE__; goto ret; }
    for ( int i = 0 ; i < NUM_BUFFER ; i ++ ) {
        if ( ion_alloc( ion_client, bufferSize[i], 0, 1, 0, &ion_hnd[i] ) ) { errLine = __LINE__; goto ret;} //1(ION_HEAP_SYSTEM_MASK)
        if ( ion_share( ion_client, ion_hnd[i], &shareFd[i] ) ) { errLine = __LINE__; goto ret; };
        if ( (vaddr[i] = mmap(NULL, bufferSize[i], map_mask, MAP_SHARED, shareFd[i], 0) ) == MAP_FAILED ) { errLine = __LINE__; goto ret; }
        //...buffer[] <-- clear
        clear( vaddr[i], bufferSize[i], paramBuff[i][2]? 0 : color[i], paramBuff[i][0] );
    }


    #if 0
    {
        // config overlay
        // fb_info <--
        //...fb_info.win_par[] <--
        int const   pos[NUM_BUFFER][2] = { {0,0}, {64,64} }; // x,y
        int const   widthSrc[NUM_BUFFER] = { BUFF0_WIDTH, BUFF1_WIDTH };
        int const   heightSrc[NUM_BUFFER] = { BUFF0_HEIGHT, BUFF1_HEIGHT };
        int const   widthDst[NUM_BUFFER] = { BUFF0_WIDTH, BUFF1_WIDTH };
        int const   heightDst[NUM_BUFFER] = { BUFF0_HEIGHT, BUFF1_HEIGHT };

        assert( NUM_BUFFER == 2 );
        for ( int i = 0 ; i < NUM_BUFFER ; i ++ ) {
            struct rk_fb_win_cfg_data fb_info ;
            //...clear
            memset(&fb_info, 0, sizeof(fb_info));
            fb_info.ret_fence_fd = -1;
            for ( int j = 0 ; j < VOP_WIN_NUM ; j++ ) {
                fb_info.rel_fence_fd[j] = -1;
            }
            struct rk_fb_win_par& win = fb_info.win_par[0];

            win.win_id = i;
            win.z_order = i;
            //win.alpha_mode = paramBuff[i][2];
            //win.g_alpha_val = paramBuff[i][3];;

            win.area_par[0].data_format = paramBuff[i][1];
            win.area_par[0].ion_fd = shareFd[i];
            win.area_par[0].acq_fence_fd = -1;
            //...destination
            win.area_par[0].xpos = pos[i][0];
            win.area_par[0].ypos = pos[i][1];
            win.area_par[0].xsize = widthDst[i];
            win.area_par[0].ysize = heightDst[i];
            //...source
            win.area_par[0].x_offset = 0;
            win.area_par[0].xact = widthSrc[i];
            win.area_par[0].yact = heightSrc[i];
            uint32_t  wVir, hVir, yOfs;
            win.area_par[0].xvir = widthSrc[i];
            win.area_par[0].yvir = heightSrc[i];
            win.area_par[0].y_offset = 0;

            // ioctl <-- fb_info
            if ( ioctl( fdFB[i], RK_FBIOSET_CONFIG_DONE, &fb_info ) ) { errLine = __LINE__; goto ret; }

            // close rel_fence_fd[] and ret_fence_fd
            for ( int j = 0 ; j < RK_MAX_BUF_NUM ; j ++ ) {
                if ( fb_info.rel_fence_fd[j] > -1 )
                    close( fb_info.rel_fence_fd[j] );
            }
            if ( fb_info.ret_fence_fd > -1 )
                close( fb_info.ret_fence_fd );
        }
    }
    #else
    {
        // config overlay
        // fb_info <--
        struct rk_fb_win_cfg_data fb_info ;
        //...clear
        memset(&fb_info, 0, sizeof(fb_info));
        fb_info.ret_fence_fd = -1;
        for ( int i = 0 ; i < VOP_WIN_NUM ; i++ ) {
            fb_info.rel_fence_fd[i] = -1;
        }
        //...fb_info.win_par[] <--
        int const   pos[NUM_BUFFER][2] = { {0,0}, {64,64} }; // x,y
        int const   widthSrc[NUM_BUFFER] = { BUFF0_WIDTH, BUFF1_WIDTH };
        int const   heightSrc[NUM_BUFFER] = { BUFF0_HEIGHT, BUFF1_HEIGHT };
        int const   widthDst[NUM_BUFFER] = { BUFF0_WIDTH, BUFF1_WIDTH };
        int const   heightDst[NUM_BUFFER] = { BUFF0_HEIGHT, BUFF1_HEIGHT };

        assert( NUM_BUFFER == 2 );
        for ( int i = 0 ; i < NUM_BUFFER ; i ++ ) {
            struct rk_fb_win_par& win = fb_info.win_par[i];

            win.win_id = i;
            win.z_order = i;
            //win.alpha_mode = paramBuff[i][2];
            //win.g_alpha_val = paramBuff[i][3];;

            win.area_par[0].data_format = paramBuff[i][1];
            win.area_par[0].ion_fd = shareFd[i];
            win.area_par[0].acq_fence_fd = -1;
            //...destination
            win.area_par[0].xpos = pos[i][0];
            win.area_par[0].ypos = pos[i][1];
            win.area_par[0].xsize = widthDst[i];
            win.area_par[0].ysize = heightDst[i];
            //...source
            win.area_par[0].x_offset = 0;
            win.area_par[0].xact = widthSrc[i];
            win.area_par[0].yact = heightSrc[i];
            uint32_t  wVir, hVir, yOfs;
            win.area_par[0].xvir = widthSrc[i];
            win.area_par[0].yvir = heightSrc[i];
            win.area_par[0].y_offset = 0;
        }

        if ( paramCommon[0] ) {
          // swap win0 & win1
          int     swap = paramCommon[0];
          if ( ioctl( fdFB[0], RK_FBIOSET_OVERLAY_STA, &swap ) ) { errLine = __LINE__; goto ret; }
          printf("swap done=%d\n", swap );
        }

        // ioctl <-- fb_info
        if ( ioctl( fdFB[0], RK_FBIOSET_CONFIG_DONE, &fb_info ) ) { errLine = __LINE__; goto ret; }


        if ( paramCommon[0] ) {
          // swap win0 & win1
          int     swap = paramCommon[0];
          if ( ioctl( fdFB[0], RK_FBIOSET_OVERLAY_STA, &swap ) ) { errLine = __LINE__; goto ret; }
          printf("swap done=%d\n", swap );
        }

        {
            int   swap = -1;
            if ( ioctl( fdFB[0], RK_FBIOGET_OVERLAY_STA, &swap ) ) { errLine = __LINE__; goto ret; }
            printf("swap=%d\n", swap );
        }
     
        // close rel_fence_fd[] and ret_fence_fd
        for ( int i = 0 ; i < RK_MAX_BUF_NUM ; i ++ ) {
            if ( fb_info.rel_fence_fd[i] > -1 )
                close( fb_info.rel_fence_fd[i] );
        }
        if ( fb_info.ret_fence_fd > -1 )
            close( fb_info.ret_fence_fd );
    }
    #endif
    

  ret:
    if ( errLine > 0 ) {
        printf("error: line=%d, %s\n", errLine, strerror(errno) );
    } else
        printf("OK\n");
    // ..fb
    if (fdFB[0]) close(fdFB[0]);
    if (fdFB[1]) close(fdFB[1]);
    //...ion
    for ( int i = 0 ; i < NUM_BUFFER ; i ++ ) {
        if (shareFd[i]) close(shareFd[i]);
        if (ion_hnd[i]) ion_free(ion_client,ion_hnd[i]);
    }
    if (ion_client)
        ion_close( ion_client );

    //sleep( 2000 );
    return 0;
}


