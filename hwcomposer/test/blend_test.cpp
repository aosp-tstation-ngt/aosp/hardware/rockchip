#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/fb.h>
#include <stdlib.h>
#include <sys/mman.h>

#include  <limits.h>
#include  <stdio.h>
#include  <time.h>

#define ALOG(fmt,...) printf(fmt "\n",__VA_ARGS__)
#define ALOG0(fmt) printf(fmt "\n")
#define RGA_BLIT_SYNC	0x5017
#define RGA_BLIT_ASYNC  0x5018
#define RGA_FLUSH       0x5019
#define RGA_GET_RESULT  0x501a
#define RGA_GET_VERSION 0x501b
#define RGA_REG_CTRL_LEN    0x8    /* 8  */
#define RGA_REG_CMD_LEN     0x1c   /* 28 */
#define RGA_CMD_BUF_SIZE    0x700  /* 16*28*4 */


typedef struct hwc_rect {
    int left;
    int top;
    int right;
    int bottom;
} hwc_rect_t;

/* RGA process mode enum */
//@@@@  (rga_req.render_mode )
enum
{    
    bitblt_mode               = 0x0,
    color_palette_mode        = 0x1,
    color_fill_mode           = 0x2,
    line_point_drawing_mode   = 0x3,
    blur_sharp_filter_mode    = 0x4,
    pre_scaling_mode          = 0x5,
    update_palette_table_mode = 0x6,
    update_patten_buff_mode   = 0x7,
};


enum
{
    rop_enable_mask          = 0x2,
    dither_enable_mask       = 0x8,
    fading_enable_mask       = 0x10,
    PD_enbale_mask           = 0x20,
};

enum
{
    yuv2rgb_mode0            = 0x0,     /* BT.601 MPEG */
    yuv2rgb_mode1            = 0x1,     /* BT.601 JPEG */
    yuv2rgb_mode2            = 0x2,     /* BT.709      */
};


/* RGA rotate mode */
enum 
{
    rotate_mode0             = 0x0,     /* no rotate */
    rotate_mode1             = 0x1,     /* rotate    */
    rotate_mode2             = 0x2,     /* x_mirror  */
    rotate_mode3             = 0x3,     /* y_mirror  */
};

enum
{
    color_palette_mode0      = 0x0,     /* 1K */
    color_palette_mode1      = 0x1,     /* 2K */
    color_palette_mode2      = 0x2,     /* 4K */
    color_palette_mode3      = 0x3,     /* 8K */
};

enum 
{
    BB_BYPASS   = 0x0,     /* no rotate */
    BB_ROTATE   = 0x1,     /* rotate    */
    BB_X_MIRROR = 0x2,     /* x_mirror  */
    BB_Y_MIRROR = 0x3      /* y_mirror  */
};

enum 
{
    nearby   = 0x0,     /* no rotate */
    bilinear = 0x1,     /* rotate    */
    bicubic  = 0x2,     /* x_mirror  */
};


/*
//          Alpha    Red     Green   Blue  
{  4, 32, {{32,24,   8, 0,  16, 8,  24,16 }}, GGL_RGBA },   // RK_FORMAT_RGBA_8888    
{  4, 24, {{ 0, 0,   8, 0,  16, 8,  24,16 }}, GGL_RGB  },   // RK_FORMAT_RGBX_8888    
{  3, 24, {{ 0, 0,   8, 0,  16, 8,  24,16 }}, GGL_RGB  },   // RK_FORMAT_RGB_888
{  4, 32, {{32,24,  24,16,  16, 8,   8, 0 }}, GGL_BGRA },   // RK_FORMAT_BGRA_8888
{  2, 16, {{ 0, 0,  16,11,  11, 5,   5, 0 }}, GGL_RGB  },   // RK_FORMAT_RGB_565        
{  2, 16, {{ 1, 0,  16,11,  11, 6,   6, 1 }}, GGL_RGBA },   // RK_FORMAT_RGBA_5551    
{  2, 16, {{ 4, 0,  16,12,  12, 8,   8, 4 }}, GGL_RGBA },   // RK_FORMAT_RGBA_4444
{  3, 24, {{ 0, 0,  24,16,  16, 8,   8, 0 }}, GGL_BGR  },   // RK_FORMAT_BGB_888

*/
typedef enum _Rga_SURF_FORMAT
{
	RK_FORMAT_RGBA_8888    = 0x0,
    RK_FORMAT_RGBX_8888    = 0x1,
    RK_FORMAT_RGB_888      = 0x2,
    RK_FORMAT_BGRA_8888    = 0x3,
    RK_FORMAT_RGB_565      = 0x4,
    RK_FORMAT_RGBA_5551    = 0x5,
    RK_FORMAT_RGBA_4444    = 0x6,
    RK_FORMAT_BGR_888      = 0x7,
    
    RK_FORMAT_YCbCr_422_SP = 0x8,    
    RK_FORMAT_YCbCr_422_P  = 0x9,    
    RK_FORMAT_YCbCr_420_SP = 0xa,    
    RK_FORMAT_YCbCr_420_P  = 0xb,

    RK_FORMAT_YCrCb_422_SP = 0xc,    
    RK_FORMAT_YCrCb_422_P  = 0xd,    
    RK_FORMAT_YCrCb_420_SP = 0xe,    
    RK_FORMAT_YCrCb_420_P  = 0xf,
    
    RK_FORMAT_BPP1         = 0x10,
    RK_FORMAT_BPP2         = 0x11,
    RK_FORMAT_BPP4         = 0x12,
    RK_FORMAT_BPP8         = 0x13,
    RK_FORMAT_YCbCr_420_SP_10B = 0x20,
    RK_FORMAT_YCrCb_420_SP_10B = 0x21,
    RK_FORMAT_UNKNOWN       = 0x100, 
}RgaSURF_FORMAT;
    
    
typedef struct rga_img_info_t
{
    unsigned int yrgb_addr;      /* yrgb    mem addr         */
    unsigned int uv_addr;        /* cb/cr   mem addr         */
    unsigned int v_addr;         /* cr      mem addr         */
    unsigned int format;         //definition by RK_FORMAT
    unsigned short act_w;
    unsigned short act_h;
    unsigned short x_offset;
    unsigned short y_offset;
    
    unsigned short vir_w;
    unsigned short vir_h;
    
    unsigned short endian_mode; //for BPP
    unsigned short alpha_swap;
}
rga_img_info_t;


typedef struct mdp_img_act
{
    unsigned short w;         // width
    unsigned short h;         // height
    short x_off;     // x offset for the vir
    short y_off;     // y offset for the vir
}
mdp_img_act;



typedef struct RANGE
{
    unsigned short min;
    unsigned short max;
}
RANGE;

typedef struct POINT
{
    unsigned short x;
    unsigned short y;
}
POINT;

typedef struct RECT
{
    unsigned short xmin;
    unsigned short xmax; // width - 1
    unsigned short ymin; 
    unsigned short ymax; // height - 1 
} RECT;

typedef struct RGB
{
    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char res;
}RGB;


typedef struct MMU
{
    unsigned char mmu_en;
    unsigned int base_addr;
    unsigned int mmu_flag;     /* [0] mmu enable [1] src_flush [2] dst_flush [3] CMD_flush [4~5] page size*/
} MMU;




typedef struct COLOR_FILL
{
    short gr_x_a;
    short gr_y_a;
    short gr_x_b;
    short gr_y_b;
    short gr_x_g;
    short gr_y_g;
    short gr_x_r;
    short gr_y_r;

    //u8  cp_gr_saturation;
}
COLOR_FILL;

typedef struct FADING
{
    unsigned char b;
    unsigned char g;
    unsigned char r;
    unsigned char res;
}
FADING;


typedef struct line_draw_t
{
    POINT start_point;              /* LineDraw_start_point                */
    POINT end_point;                /* LineDraw_end_point                  */
    unsigned int   color;               /* LineDraw_color                      */
    unsigned int   flag;                /* (enum) LineDrawing mode sel         */
    unsigned int   line_width;          /* range 1~16 */
}
line_draw_t;



struct rga_req { 
    unsigned char render_mode;            /* (enum) process mode sel */
    
    rga_img_info_t src;                   /* src image info */
    rga_img_info_t dst;                   /* dst image info */
    rga_img_info_t pat;             /* patten image info */

    unsigned int rop_mask_addr;         /* rop4 mask addr */
    unsigned int LUT_addr;              /* LUT addr */
    RECT clip;                      /* dst clip window default value is dst_vir */
                                    /* value from [0, w-1] / [0, h-1]*/
        
    int sina;                   /* dst angle  default value 0  16.16 scan from table */
    int cosa;                   /* dst angle  default value 0  16.16 scan from table */        

    unsigned short alpha_rop_flag;        /* alpha rop process flag           */
                                    /* ([0] = 1 alpha_rop_enable)       */
                                    /* ([1] = 1 rop enable)             */                                                                                                                
                                    /* ([2] = 1 fading_enable)          */
                                    /* ([3] = 1 PD_enable)              */
                                    /* ([4] = 1 alpha cal_mode_sel)     */
                                    /* ([5] = 1 dither_enable)          */
                                    /* ([6] = 1 gradient fill mode sel) */
                                    /* ([7] = 1 AA_enable)              */

    unsigned char  scale_mode;            /* 0 nearst / 1 bilnear / 2 bicubic */                             
                            
    unsigned int color_key_max;         /* color key max */
    unsigned int color_key_min;         /* color key min */     

    unsigned int fg_color;              /* foreground color */
    unsigned int bg_color;              /* background color */

    COLOR_FILL gr_color;            /* color fill use gradient */
    
    line_draw_t line_draw_info;
    
    FADING fading;
                              
    unsigned char PD_mode;                /* porter duff alpha mode sel */
    
    unsigned char alpha_global_value;     /* global alpha value */
     
    unsigned short rop_code;              /* rop2/3/4 code  scan from rop code table*/
    
    unsigned char bsfilter_flag;          /* [2] 0 blur 1 sharp / [1:0] filter_type*/
    
    unsigned char palette_mode;           /* (enum) color palatte  0/1bpp, 1/2bpp 2/4bpp 3/8bpp*/

    unsigned char yuv2rgb_mode;           /* (enum) BT.601 MPEG / BT.601 JPEG / BT.709  */ 
    
    unsigned char endian_mode;            /* 0/big endian 1/little endian*/

    unsigned char rotate_mode;            /* (enum) rotate mode  */
                                    /* 0x0,     no rotate  */
                                    /* 0x1,     rotate     */
                                    /* 0x2,     x_mirror   */
                                    /* 0x3,     y_mirror   */

    unsigned char color_fill_mode;        /* 0 solid color / 1 patten color */
                                    
    MMU mmu_info;                   /* mmu information */

    unsigned char  alpha_rop_mode;        /* ([0~1] alpha mode)       */
                                    /* ([2~3] rop   mode)       */
                                    /* ([4]   zero  mode en)    */
                                    /* ([5]   dst   alpha mode) */

    unsigned char  src_trans_mode;

    unsigned char CMD_fin_int_enable;                        

    /* completion is reported through a callback */
	void (*complete)(int retval);
};


int
RGA_set_dst_act_info(
		struct rga_req *req,
		unsigned int   width,       /* act width  */
		unsigned int   height,      /* act height */
		unsigned int   x_off,       /* x_off      */
		unsigned int   y_off        /* y_off      */
		);

int
RGA_set_dst_vir_info(
		struct rga_req *msg,
		unsigned int   yrgb_addr,   /* yrgb_addr   */
		unsigned int   uv_addr,     /* uv_addr     */
		unsigned int   v_addr,      /* v_addr      */
		unsigned int   vir_w,       /* vir width   */
		unsigned int   vir_h,       /* vir height  */
		RECT           *clip,        /* clip window */
		unsigned char  format,      /* format      */
		unsigned char  a_swap_en
		);

int 
RGA_set_pat_info(
    struct rga_req *msg,
    unsigned int width,
    unsigned int height,
    unsigned int x_off,
    unsigned int y_off,
    unsigned int pat_format    
    );

int
RGA_set_rop_mask_info(
		struct rga_req *msg,
		unsigned int rop_mask_addr,
		unsigned int rop_mask_endian_mode
		);
   

int RGA_set_alpha_en_info(
		struct rga_req *msg,

		unsigned int  alpha_en,    
		unsigned int  alpha_cal_mode,    /* 0:alpha' = alpha + (alpha>>7) | alpha' = alpha */
		unsigned int  alpha_mode,        /* 0 global alpha / 1 per pixel alpha / 2 mix mode */
		unsigned int  global_a_value,
		unsigned int  PD_en,             /* porter duff alpha mode en */ 
		unsigned int  PD_mode, 
		unsigned int  ZERO_en,        //@@@@ zero mode??
		unsigned int  dst_alpha_en )     /* use dst alpha  */
{
	
    msg->alpha_rop_flag |= alpha_en; //@@@@1;
    msg->alpha_rop_flag |= ((PD_en & 1) << 3);
    msg->alpha_rop_flag |= ((alpha_cal_mode & 1) << 4);
    msg->alpha_global_value = global_a_value;
    msg->alpha_rop_mode |= (alpha_mode & 3);    
    msg->alpha_rop_mode |= ZERO_en << 4;      //@@@@
    msg->alpha_rop_mode |= (dst_alpha_en << 5);
    msg->PD_mode = PD_mode;
    
    
    return 1;
}

int
RGA_set_rop_en_info(
		struct rga_req *msg,
		unsigned int ROP_mode,
		unsigned int ROP_code,
		unsigned int color_mode,
		unsigned int solid_color
		);

 
int
RGA_set_fading_en_info(
		struct rga_req *msg,
		unsigned char r,
		unsigned char g,
		unsigned char b
		);

int
RGA_set_src_trans_mode_info(
		struct rga_req *msg,
		unsigned char trans_mode,
		unsigned char a_en,
		unsigned char b_en,
		unsigned char g_en,
		unsigned char r_en,
		unsigned char color_key_min,
		unsigned char color_key_max,
		unsigned char zero_mode_en
		);


int
RGA_set_bitblt_mode(
		struct rga_req *msg,
		unsigned char scale_mode,    // 0/near  1/bilnear  2/bicubic  
		unsigned char rotate_mode,   // 0/copy 1/rotate_scale 2/x_mirror 3/y_mirror 
		unsigned int  angle,         // rotate angle     
		unsigned int  dither_en,     // dither en flag   
		unsigned int  AA_en,         // AA flag          
		unsigned int  yuv2rgb_mode
		)
{
    unsigned int alpha_mode;
    msg->render_mode = bitblt_mode;

    //@@@@if(((msg->src.act_w >> 1) > msg->dst.act_w) || ((msg->src.act_h >> 1) > msg->dst.act_h))
    //@@@@  return -1;
    
    msg->scale_mode = scale_mode;
    msg->rotate_mode = rotate_mode;
    
    //@@@@
    msg->sina = 0;//@@@@ sina_table[angle];
    msg->cosa = 65536; //@@@@cosa_table[angle];

    msg->yuv2rgb_mode = yuv2rgb_mode;

    msg->alpha_rop_flag |= ((AA_en << 7) & 0x80);

    alpha_mode = msg->alpha_rop_mode & 3;
    if(rotate_mode == BB_ROTATE)
    {
      if (AA_en == 1/*ENABLE*/) 
        {   
            if ((msg->alpha_rop_flag & 0x3) == 0x1)
            {
                if (alpha_mode == 0)
                {
                msg->alpha_rop_mode = 0x2;            
                }
                else if (alpha_mode == 1)
                {
                    msg->alpha_rop_mode = 0x1;
                }
            }
            else
            {
                msg->alpha_rop_flag |= 1;
                msg->alpha_rop_mode = 1;
            }                        
        }        
    }
   
    if (msg->src_trans_mode)
        msg->scale_mode = 0;

    msg->alpha_rop_flag |= (dither_en << 5);
    
    return 0;
}


int
RGA_set_color_palette_mode(
		struct rga_req *msg,
		unsigned char  palette_mode,        /* 1bpp/2bpp/4bpp/8bpp */
		unsigned char  endian_mode,         /* src endian mode sel */
		unsigned int  bpp1_0_color,         /* BPP1 = 0 */
		unsigned int  bpp1_1_color          /* BPP1 = 1 */
		);


int
RGA_set_color_fill_mode(
		struct rga_req *msg,
		COLOR_FILL  *gr_color,                    /* gradient color part         */
		unsigned char  gr_satur_mode,            /* saturation mode             */
    	unsigned char  cf_mode,                  /* patten fill or solid fill   */
		unsigned int color,                      /* solid color                 */
		unsigned short pat_width,                /* pattern width               */
		unsigned short pat_height,               /* pattern height              */   
		unsigned char pat_x_off,                 /* pattern x offset            */
		unsigned char pat_y_off,                 /* pattern y offset            */
		unsigned char aa_en                      /* alpha en                    */
		);


int
RGA_set_line_point_drawing_mode(
		struct rga_req *msg,
		POINT sp,                     /* start point              */
		POINT ep,                     /* end   point              */
		unsigned int color,           /* line point drawing color */
		unsigned int line_width,      /* line width               */
		unsigned char AA_en,          /* AA en                    */
		unsigned char last_point_en   /* last point en            */
		);


int
RGA_set_blur_sharp_filter_mode(
		struct rga_req *msg,
		unsigned char filter_mode,   /* blur/sharpness   */
		unsigned char filter_type,   /* filter intensity */
		unsigned char dither_en      /* dither_en flag   */
		);

int
RGA_set_pre_scaling_mode(
		struct rga_req *msg,
		unsigned char dither_en
		);

int
RGA_update_palette_table_mode(
		struct rga_req *msg,
		unsigned int LUT_addr,      /* LUT table addr      */
		unsigned int palette_mode   /* 1bpp/2bpp/4bpp/8bpp */
		);

int
RGA_set_update_patten_buff_mode(
		struct rga_req *msg,
		unsigned int pat_addr, /* patten addr    */
		unsigned int w,        /* patten width   */
		unsigned int h,        /* patten height  */
		unsigned int format    /* patten format  */
		);

int
RGA_set_mmu_info(
		struct rga_req *msg,
		unsigned char  mmu_en,
		unsigned char  src_flush,
		unsigned char  dst_flush,
		unsigned char  cmd_flush,
		unsigned int base_addr,
		unsigned char  page_size
		);

void rga_set_fds_offsets(
        struct rga_req *rga_request,
        unsigned short src_fd,
        unsigned short dst_fd,
        unsigned int src_offset, 
        unsigned int dst_offset);

int
RGA_set_src_fence_flag(
    struct rga_req *msg,
    int acq_fence,
    int src_flag
);


int
RGA_set_dst_fence_flag(
    struct rga_req *msg,
    int dst_flag
);

int
RGA_get_dst_fence(
    struct rga_req *msg
);

//#define PAGE_SIZE 4096
inline size_t round_up_to_page_size(size_t x)
{
	return (x + (PAGE_SIZE - 1)) & ~(PAGE_SIZE - 1);
}


void rga_set_fds_offsets(struct rga_req *rga_request,
			 uint16_t src_fd, uint16_t dst_fd,
			 uint32_t src_offset, uint32_t dst_offset)
{
       rga_request->line_draw_info.color = src_fd | (dst_fd << 16);
       rga_request->line_draw_info.flag = src_offset;
       rga_request->line_draw_info.line_width = dst_offset;
}


int
RGA_set_mmu_info(
		struct rga_req *msg,
		unsigned char  mmu_en,
		unsigned char  src_flush,
		unsigned char  dst_flush,
		unsigned char  cmd_flush,
		unsigned int base_addr,
		unsigned char  page_size
		)
{
    msg->mmu_info.mmu_en    = mmu_en;
    msg->mmu_info.base_addr = base_addr;
    msg->mmu_info.mmu_flag  = ((page_size & 0x3) << 4) |
                              ((cmd_flush & 0x1) << 3) |
                              ((dst_flush & 0x1) << 2) | 
                              ((src_flush & 0x1) << 1) | mmu_en;
    return 1;
}

int
RGA_set_color_fill_mode(
		struct rga_req *msg,
		COLOR_FILL  *gr_color,              /* gradient color part         */
		unsigned char  gr_satur_mode,            /* saturation mode             */
    	unsigned char  cf_mode,                  /* patten fill or solid fill   */
		unsigned int color,                    /* solid color                 */
		unsigned short pat_width,                /* pattern width               */
		unsigned short pat_height,               /* pattern height              */   
		unsigned char pat_x_off,                 /* pattern x offset            */
		unsigned char pat_y_off,                 /* pattern y offset            */
		unsigned char aa_en                      /* alpha en                    */
		)
{
    msg->render_mode = color_fill_mode;

    msg->gr_color.gr_x_a = ((int)(gr_color->gr_x_a * 256.0))& 0xffff;
    msg->gr_color.gr_x_b = ((int)(gr_color->gr_x_b * 256.0))& 0xffff;
    msg->gr_color.gr_x_g = ((int)(gr_color->gr_x_g * 256.0))& 0xffff;
    msg->gr_color.gr_x_r = ((int)(gr_color->gr_x_r * 256.0))& 0xffff;

    msg->gr_color.gr_y_a = ((int)(gr_color->gr_y_a * 256.0))& 0xffff;
    msg->gr_color.gr_y_b = ((int)(gr_color->gr_y_b * 256.0))& 0xffff;
    msg->gr_color.gr_y_g = ((int)(gr_color->gr_y_g * 256.0))& 0xffff;
    msg->gr_color.gr_y_r = ((int)(gr_color->gr_y_r * 256.0))& 0xffff;

    msg->color_fill_mode = cf_mode;
    
    msg->pat.act_w = pat_width;
    msg->pat.act_h = pat_height;

    msg->pat.x_offset = pat_x_off;
    msg->pat.y_offset = pat_y_off;

    msg->fg_color = color;

    msg->alpha_rop_flag |= ((gr_satur_mode & 1) << 6);
    
    if(aa_en)
    {
    	msg->alpha_rop_flag |= 0x1;
    	msg->alpha_rop_mode  = 1;
    }
    return 1;
}

int
RGA_set_dst_act_info(
		struct rga_req *req,
		unsigned int   width,       /* act width  */
		unsigned int   height,      /* act height */
		unsigned int   x_off,       /* x_off      */
		unsigned int   y_off        /* y_off      */
		)
{
    req->dst.act_w = width;
    req->dst.act_h = height;
    req->dst.x_offset = x_off;
    req->dst.y_offset = y_off;
    
    return 1;
}

int
RGA_set_src_act_info(
		struct rga_req *req,
		unsigned int   width,       /* act width  */
		unsigned int   height,      /* act height */
		unsigned int   x_off,       /* x_off      */
		unsigned int   y_off        /* y_off      */
		)
{
    req->src.act_w = width;
    req->src.act_h = height;
    req->src.x_offset = x_off;
    req->src.y_offset = y_off;
    
    return 1;
}

int
RGA_set_dst_vir_info(
		struct rga_req *msg,
		unsigned int   yrgb_addr,   /* yrgb_addr   */
		unsigned int   uv_addr,     /* uv_addr     */
		unsigned int   v_addr,      /* v_addr      */
		unsigned int   vir_w,       /* vir width   */
		unsigned int   vir_h,       /* vir height  */
		RECT           *clip,        /* clip window */
		unsigned char  format,      /* format      */
		unsigned char  a_swap_en
		)
{
    msg->dst.yrgb_addr = yrgb_addr;
    msg->dst.uv_addr  = uv_addr;
    msg->dst.v_addr   = v_addr;
    msg->dst.vir_w = vir_w;
    msg->dst.vir_h = vir_h;
    msg->dst.format = format;

    msg->clip.xmin = clip->xmin;
    msg->clip.xmax = clip->xmax;
    msg->clip.ymin = clip->ymin;
    msg->clip.ymax = clip->ymax;
    
    msg->dst.alpha_swap |= (a_swap_en & 1);
    
    return 1;
}

int
RGA_set_src_vir_info(
		struct rga_req *req,
		unsigned int   yrgb_addr,       /* yrgb_addr  */
		unsigned int   uv_addr,         /* uv_addr    */
		unsigned int   v_addr,          /* v_addr     */
		unsigned int   vir_w,           /* vir width  */
		unsigned int   vir_h,           /* vir height */
		unsigned char  format,          /* format     */
		unsigned char  a_swap_en        /* only for 32bit RGB888 format */
		)
{
    req->src.yrgb_addr = yrgb_addr;
    req->src.uv_addr  = uv_addr;
    req->src.v_addr   = v_addr;
    req->src.vir_w = vir_w;
    req->src.vir_h = vir_h;
    req->src.format = format;
    req->src.alpha_swap |= (a_swap_en & 1);

    return 1;
}


void swap( int& v1, int& v2 )
{
  int   tmp = v1;
  v1 = v2;
  v2 = tmp;
}


//----------------------------------------------------------
// screen infomation (buffer size, etc..)
//----------------------------------------------------------
#define PIXEL_FORMAT          RK_FORMAT_RGBA_8888
#define PIXEL_SIZE_IN_BYTES   4

#define SCREEN_WIDTH    1024
#define SCREEN_HEIGHT   600

#define SCREEN_STRIDE         SCREEN_WIDTH
#define NUM_BUFFER            3

//----------------------------------------------------------
// hwcBlit
//----------------------------------------------------------

// for test
struct g_ {
  int   srcLeft, srcTop, srcWidth, srcHeight;
} g;

typedef unsigned short  uint16_t;
void hwcBlit( int fdRGA,
              hwc_rect_t const& rectSrc,
              hwc_rect_t const& rectDst,
              int share_fdSrc,
              int share_fdDst,
              int srcHeightVir,
              int alphaParam[8] )
{

  int        dstWidth  = rectDst.right - rectDst.left;
  int        dstHeight = rectDst.bottom - rectDst.top;
  int        dstStride = SCREEN_STRIDE;
  RgaSURF_FORMAT      dstFormat = RK_FORMAT_RGBA_8888;
  unsigned int        dstHeightVir = 600*3;


  int        srcWidth  = rectSrc.right - rectSrc.left;
  int        srcHeight = rectSrc.bottom - rectSrc.top;
  int        srcStride = SCREEN_STRIDE;
  RgaSURF_FORMAT      srcFormat = PIXEL_FORMAT;
  //unsigned int        srcHeightVir = SCREEN_HEIGHT*NUM_BUFFER;

  //unsigned int        yOffsetCommon = offset / (PIXEL_SIZE_IN_BYTES*SCREEN_STRIDE);



  //RECT                clip = { (uint16_t)rectDst.left, (uint16_t)(rectDst.right-1), (uint16_t)(rectDst.top + yOffsetCommon), (uint16_t)(rectDst.bottom-1 + yOffsetCommon ) };
  RECT                clip = { 0, (uint16_t)(dstStride-1), 0, (uint16_t)(dstHeightVir-1) };
    //unsigned short xmin;
    //unsigned short xmax; // width - 1
    //unsigned short ymin; 
    //unsigned short ymax; // height - 1 
  struct rga_req  Rga_Request;

  memset(&Rga_Request, 0x0, sizeof(Rga_Request));
  //RGA_set_src_vir_info(&Rga_Request, share_fdSrc, (intptr_t)vaddr,  0, srcStride, srcHeightVir, srcFormat, 0);
  //RGA_set_dst_vir_info(&Rga_Request, share_fdDst, (intptr_t)vaddr,  0, dstStride, dstHeightVir, &clip, dstFormat, 0);

  RGA_set_src_vir_info(&Rga_Request, share_fdSrc, 0,  0, srcStride, srcHeightVir, srcFormat, 0);
  RGA_set_dst_vir_info(&Rga_Request, share_fdDst, 0,  0, dstStride, dstHeightVir, &clip, dstFormat, 0);

  //(NG) RGA_set_src_vir_info(&Rga_Request, (intptr_t)vaddr, 0,  0, srcStride, srcHeightVir, srcFormat, 0);
  //(NG) RGA_set_dst_vir_info(&Rga_Request, (intptr_t)vaddr, 0,  0, dstStride, dstHeightVir, &clip, dstFormat, 0);
  rga_set_fds_offsets(&Rga_Request, 0, 0, 0, 0);


  // rotation
  int   dither_en = 0; // always 0 : android::bytesPerPixel(GPU_FORMAT) == android::bytesPerPixel(GPU_DST_FORMAT) ? 0 : 1;
  int   scale_mode = 0; // 0/near  1/bilnear  2/bicubic : 0(non if srcFormat == RK_FORMAT_RGBA_8888 )
  int   RotateMode = 0; // 0/copy 1/rotate_scale 2/x_mirror 3/y_mirror 
  int   Rotation = 0; 
  int   srcOffsetX = rectSrc.left, dstOffsetX = rectDst.left;
  int   srcOffsetY = rectSrc.top, dstOffsetY = rectDst.top;

  // alpha
  if ( alphaParam[0] ) {
      RGA_set_alpha_en_info(&Rga_Request, alphaParam[0], alphaParam[1], alphaParam[2] , alphaParam[3], alphaParam[4], alphaParam[5], alphaParam[6], alphaParam[7] );
  } else {
      ALOG0("@@@ copy");
  }

  RGA_set_src_act_info(&Rga_Request, srcWidth, srcHeight, srcOffsetX, srcOffsetY);
  RGA_set_dst_act_info(&Rga_Request, dstWidth, dstHeight, dstOffsetX, dstOffsetY);
  RGA_set_bitblt_mode(&Rga_Request, scale_mode, RotateMode, Rotation, dither_en, 0, 0);

  int srcMmuType = 1;
  int dstMmuType = 1;
  RGA_set_mmu_info(&Rga_Request, 1, 0, 0, 0, 0, 2);
  Rga_Request.mmu_info.mmu_flag |= (1 << 31) | (dstMmuType << 10) | (srcMmuType << 8);

  if (ioctl(fdRGA, RGA_BLIT_ASYNC, &Rga_Request) != 0) {
      ALOG("%s(%d):  RGA_BLIT_ASYNC Failed", __FUNCTION__, __LINE__);
  }
  if ( ioctl(fdRGA, RGA_FLUSH, NULL) != 0 ) {
      ALOG("%s(%d):  RGA_FLUSH Failed", __FUNCTION__, __LINE__);
  }
}

void  rgaBlit( int fdRGA,
              hwc_rect_t const& rectSrc,
              hwc_rect_t const& rectDst,

              hwc_rect_t const& rectSrcVir,
              hwc_rect_t const& rectDstVir,

              int share_fdSrc,
              int share_fdDst,
              int alphaParam[9] )

{
    int retv  = 0;
    int const srcMmuType = 1;
    int const dstMmuType = 1;
    RgaSURF_FORMAT      dstFormat = RK_FORMAT_RGBA_8888;
    RgaSURF_FORMAT      srcFormat = RK_FORMAT_RGBA_8888;
    struct rga_req    Rga_Request;
    unsigned char     RotateMode = 0;
    int               Rotation = 0;
    unsigned char     scale_mode = 0; //@@@@2;
    unsigned char   dither_en = 0;

    memset(&Rga_Request, 0x0, sizeof(Rga_Request));
    RGA_set_mmu_info(&Rga_Request, 1, 0, 0, 0, 0, 2);
    Rga_Request.mmu_info.mmu_flag |= (1 << 31) | (dstMmuType << 10) | (srcMmuType << 8);

    RECT clip = { (uint16_t)rectDst.left,
                  (uint16_t)(rectDst.right - 1),
                  (uint16_t)(rectDst.top),
                  (uint16_t)(rectDst.bottom - 1) }; //xmin xmax ymin ymax
    RGA_set_src_vir_info(&Rga_Request, share_fdSrc, 0,  0, rectSrcVir.right, rectSrcVir.bottom, srcFormat, 0);
    RGA_set_dst_vir_info(&Rga_Request, share_fdDst, 0,  0, rectDstVir.right, rectDstVir.bottom, &clip, dstFormat, 0);

    // alpha blending
    if ( alphaParam[0] ) {
        RGA_set_alpha_en_info(&Rga_Request, alphaParam[0], alphaParam[1], alphaParam[2], alphaParam[3],
                              alphaParam[4], alphaParam[5], alphaParam[6], alphaParam[7] );
    } else {
        //ALOG0("@@@ copy");
    }

    scale_mode = 0;
    RGA_set_bitblt_mode(&Rga_Request, scale_mode, RotateMode, Rotation, dither_en, 0, 0);
    RGA_set_src_act_info(&Rga_Request, rectSrc.right-rectSrc.left, rectSrc.bottom-rectSrc.top,
                         rectSrc.left, rectSrc.top );
    RGA_set_dst_act_info(&Rga_Request,
                         rectDst.right - rectDst.left,
                         rectDst.bottom - rectDst.top,
                         rectDst.left,
                         rectDst.top  );//@@@@

    retv = ioctl(fdRGA, RGA_BLIT_ASYNC, &Rga_Request);
    if( retv != 0)
        ALOG("RGA ASYNC err=%d",retv );
}

//----------------------------------------------------------
// ion
//----------------------------------------------------------
typedef int ion_user_handle_t;
extern "C" {
int ion_open();
int ion_close(int fd);
int ion_alloc(int fd, size_t len, size_t align, unsigned int heap_mask,
              unsigned int flags, ion_user_handle_t *handle);
int ion_free(int fd, ion_user_handle_t handle);
int ion_map(int fd, ion_user_handle_t handle, size_t length, int prot,
            int flags, off_t offset, unsigned char **ptr, int *map_fd);
int ion_share(int fd, ion_user_handle_t handle, int *share_fd);
int ion_alloc_fd(int fd, size_t len, size_t align, unsigned int heap_mask,
                 unsigned int flags, int *handle_fd);
int ion_import(int fd, int share_fd, ion_user_handle_t *handle);
 int ion_sync_fd(int fd, int handle_fd);
int ion_get_phys(int fd, ion_user_handle_t handle, unsigned long *phys);
};


//----------------------------------------------------------
// working buffer info
//----------------------------------------------------------
#define NUM_PARAM 9
#define NUM_PROBE 6
#define BUFF0_WIDTH   256
#define BUFF0_HEIGHT  256

#define BUFF1_WIDTH   256
#define BUFF1_HEIGHT  128

//----------------------------------------------------------
// utility
//----------------------------------------------------------
// probe buffer0
void  probe( void* vaddr, int data[NUM_PROBE] )
{
    int const*      pBuff = (int const*)vaddr;
    int const     offset = BUFF0_WIDTH / 2 + BUFF0_WIDTH * 2;
    int const     iPos[NUM_PROBE] = {
        offset,
        (BUFF0_HEIGHT / 4   )*BUFF0_WIDTH + offset,
        (BUFF0_HEIGHT / 2   )*BUFF0_WIDTH - offset,
        (BUFF0_HEIGHT / 2   )*BUFF0_WIDTH + offset,
        (BUFF0_HEIGHT / 4*3 )*BUFF0_WIDTH - offset,
        (BUFF0_HEIGHT / 4*3 )*BUFF0_WIDTH + offset,
    };

    for ( int i = 0 ; i < NUM_PROBE ; i ++ ) {
        data[i] = pBuff[iPos[i]];
    }
}

void  probe( void* vaddr, char const* pMsg="" )
{
    int     data[NUM_PROBE];
    probe( vaddr, data );
    printf( "%10s ", pMsg);
    for ( int i = 0 ; i < NUM_PROBE ; i ++ )
        printf( "%8.8x ", data[i] );
    printf( "\n" );
}

void  printParam( int param[NUM_PARAM] )
{
    for ( int i = 0 ; i < NUM_PARAM ; i ++ )
        printf("%d ", param[i] );
    printf("\n");
}
void  clear( void* vaddr0, void* vaddr1 )
{
    //...buffer0 <--
    for ( int*pBuff = (int*)vaddr0, i = 0 ; i < BUFF0_HEIGHT * BUFF0_WIDTH /2 ; i ++ ) 
        pBuff[i] = 0xffff0000;
    for ( int*pBuff = (int*)vaddr0, i = BUFF0_HEIGHT * BUFF0_WIDTH /2 ; i < BUFF0_HEIGHT * BUFF0_WIDTH ; i ++ ) 
        pBuff[i] = 0x000000ff;
    //...buffer1 <--
    memset( vaddr1, 0, BUFF1_HEIGHT * BUFF1_WIDTH * sizeof(int) );
    for ( int*pBuff = (int*)vaddr1, i = 0 ; i < BUFF1_HEIGHT * BUFF1_WIDTH / 4 ; i ++ ) 
        pBuff[i] = 0xff00ff00;
    for ( int*pBuff = (int*)vaddr1, i = BUFF1_HEIGHT * BUFF1_WIDTH / 2 ; i < BUFF1_HEIGHT * BUFF1_WIDTH / 4*3 ; i ++ ) 
        pBuff[i] = 0xff00ff00;
}

int   blit( int fdRGA, int fdSrc, int fdDst, int param[NUM_PARAM] )
{
    int errLine = 0;
    //int const   yOffset = SCREEN_HEIGHT * index;
    hwc_rect_t rectBuff0 = { 0, 0, BUFF0_WIDTH, BUFF0_HEIGHT };
    hwc_rect_t rectBuff1 = { 0, 0, BUFF1_WIDTH, BUFF1_HEIGHT };
    hwc_rect_t& rectSrc = rectBuff1;
    hwc_rect_t rectDst = { 0, BUFF0_HEIGHT/4, BUFF0_WIDTH, BUFF0_HEIGHT/4 + BUFF1_HEIGHT };
    //hwc_rect_t rectDstScreen = rectBuff0;//copy ( y coordinate is modified later )
    //hwc_rect_t rectScreenVir = { 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*NUM_BUFFER };
    //int        paramCpy[NUM_PARAM];
    //memset( paramCpy, 0, sizeof(paramCpy) );  // alpha_en == 0
    //rectDstScreen.top += yOffset;
    //rectDstScreen.bottom += yOffset;
    rgaBlit( fdRGA, rectSrc, rectDst, rectSrc, rectBuff0, fdSrc, fdDst, param );
    if (ioctl(fdRGA, RGA_FLUSH, NULL) != 0) { errLine = __LINE__; goto ret; }

    return 0;
  ret:
    if ( errLine ) {
        printf( "error %d : ", errLine );
        for ( int i = 0 ; i < NUM_PARAM ; i ++ ) {
            printf( "%d ", param[i] );
        }
        printf( "\n" );
    }
    return -1;
}

int diff( int const data0[NUM_PROBE], int const data1[NUM_PROBE] )
{
    unsigned char const* pC0 = (unsigned char const*)data0;
    unsigned char const* pC1 = (unsigned char const*)data1;

    int   sum = 0;
    for ( int i = 0 ; i < (int)sizeof(int)*NUM_PROBE ; i ++ ) {
        int const d = ((int)pC0[i]&0xff) - ((int)pC1[i]&0xff);
        sum += d >= 0 ? d : -d;
    }
    return sum;
}
    
//----------------------------------------------------------
// main
//----------------------------------------------------------

int main(int argc, const char*argv[] )
{
    int       rc = 0;
    int       errLine = 0;
    int       param[NUM_PARAM];
    int const map_mask = PROT_READ | PROT_WRITE;
    // frame buffer
    int const sizeFB = SCREEN_STRIDE * SCREEN_HEIGHT * PIXEL_SIZE_IN_BYTES;  // rgba
    int const sizeFBALL = round_up_to_page_size(sizeFB * NUM_BUFFER);
    int		    fdFB = 0;
    int       shareFdFB = 0;
    int       index = 0;
    void *    vaddr;
    // blitter
    int       fdRGA = 0;
    // working buffers(0 and 1)
		int       ion_client=0;
		int       ion_hnd0=0, ion_hnd1=0, shareFd0=0, shareFd1=0;
    void*     vaddr0;//, *vaddr1;
    void*     vaddr1;//, *vaddr1;
    int const size0 = BUFF0_WIDTH * BUFF0_HEIGHT * PIXEL_SIZE_IN_BYTES;
    int const size1 = BUFF1_WIDTH * BUFF1_HEIGHT * PIXEL_SIZE_IN_BYTES;

    // param[] <-- cmd line
    if ( argc != NUM_PARAM + 1 ) {
        printf("usage : %s alpha_en alpha_cal_mode alpha_mode g_alpha PD_en PD_mode ZERO_en dst_alpha_en index\n", argv[0] );
        printf("  %d parameters are needed\n", NUM_PARAM );
        //unsigned int  alpha_cal_mode,    /* 0:alpha' = alpha + (alpha>>7) | alpha' = alpha */
        //unsigned int  alpha_mode,        /* 0 global alpha / 1 per pixel alpha / 2 mix mode */
        //unsigned int  global_a_value,
        //unsigned int  PD_en,             /* porter duff alpha mode en */ 
        //unsigned int  PD_mode, 
        //unsigned int  ZERO_EN,, 
        //unsigned int  dst_alpha_en )     /* use dst alpha  */
        errLine = __LINE__; goto ret;
    }

    //printf( "  -->" );
    for ( int i = 0 ; i < argc - 1 ; i ++ ) {
        param[i] = atoi( argv[i+1] );
        //printf("%d ", param[i] );
    }
    index = param[NUM_PARAM-1];
    //printf(" : index=%d\n", index);
    
    // shareFdFB <-- 
    fdFB = open("/dev/graphics/fb0", O_RDWR, 0);
    if ( fdFB <= 0 ) { errLine = __LINE__; goto ret; }
    //vaddr = mmap(0, sizeFBALL, map_mask, MAP_SHARED, fdFB, 0);
    //if (vaddr == MAP_FAILED) 	{ errLine = __LINE__; goto ret; }
		if ( ioctl(fdFB, /*FBIOGET_DMABUF*/0x5003, &shareFdFB) != 0 ) { errLine = __LINE__; goto ret; }

    // working buffer
		ion_client = ion_open();
    if ( ion_client <= 0 ) { errLine = __LINE__; goto ret; }
    if ( ion_alloc( ion_client, size0, 0, 1, 0, &ion_hnd0 ) ) { errLine = __LINE__; goto ret;} //1(ION_HEAP_SYSTEM_MASK)
    if ( ion_alloc( ion_client, size1, 0, 1, 0, &ion_hnd1 ) ) { errLine = __LINE__; goto ret;} //1(ION_HEAP_SYSTEM_MASK)
		if ( ion_share( ion_client, ion_hnd0, &shareFd0 ) ) { errLine = __LINE__; goto ret; };
		if ( ion_share( ion_client, ion_hnd1, &shareFd1 ) ) { errLine = __LINE__; goto ret; };
    if ( (vaddr0 = mmap(NULL, size0, map_mask, MAP_SHARED, shareFd0, 0) ) == MAP_FAILED ) { errLine = __LINE__; goto ret; }
    if ( (vaddr1 = mmap(NULL, size1, map_mask, MAP_SHARED, shareFd1, 0) ) == MAP_FAILED ) { errLine = __LINE__; goto ret; }
    //...buffer0,1 <-- clear
    clear( vaddr0, vaddr1 );


    // fdRGA <--
    fdRGA = open("/dev/rga", O_RDWR, 0);
    if ( fdRGA <= 0 ) { errLine = __LINE__; goto ret; }

#if 1
    probe( vaddr0, "before" );
    // blit( buff1-blend->buff0, buff0-copy->FB )
    {
        int const   yOffset = SCREEN_HEIGHT * index;

        hwc_rect_t rectBuff0 = { 0, 0, BUFF0_WIDTH, BUFF0_HEIGHT };
        hwc_rect_t rectBuff1 = { 0, 0, BUFF1_WIDTH, BUFF1_HEIGHT };

        hwc_rect_t& rectSrc = rectBuff1;
        hwc_rect_t rectDst = { 0, BUFF0_HEIGHT/4, BUFF0_WIDTH, BUFF0_HEIGHT/4 + BUFF1_HEIGHT };

        hwc_rect_t rectDstScreen = rectBuff0;//copy ( y coordinate is modified later )
        hwc_rect_t rectScreenVir = { 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*NUM_BUFFER };

        int        paramCpy[NUM_PARAM];

        memset( paramCpy, 0, sizeof(paramCpy) );  // alpha_en == 0

        //fdRGA = open("/dev/rga", O_RDWR, 0);
        //if ( fdRGA <= 0 ) { errLine = __LINE__; goto ret; }
        //hwcBlit( fdRGA, rectSrc, rectDst, shareFd1, shareFd0, BUFF1_HEIGHT, param );
        //hwcBlit( fdRGA, rectBuff0, rectBuff0, shareFd0, shareFdFB, BUFF0_HEIGHT, paramCpy );
        rectDstScreen.top += yOffset;
        rectDstScreen.bottom += yOffset;
        rgaBlit( fdRGA, rectSrc, rectDst, rectSrc, rectBuff0, shareFd1, shareFd0, param );
        rgaBlit( fdRGA, rectBuff0, rectDstScreen, rectBuff0, rectScreenVir, shareFd0, shareFdFB, paramCpy );

        if (ioctl(fdRGA, RGA_FLUSH, NULL) != 0) { errLine = __LINE__; goto ret; }
    }
    //sleep( 1 );
    probe( vaddr0, "after" );
#else
    {
        #define NUM_CANDI 10
        int   dataPrev[NUM_PROBE], dataCurr[NUM_PROBE];
        int   paramNear[NUM_CANDI][NUM_PARAM];
        int   diffNear[NUM_CANDI];
        int   diffTmp = INT_MAX;
        for ( int i = 0 ; i < NUM_CANDI ; i ++ ) {
            diffNear[i] = INT_MAX;
            for ( int j = 0 ; j < NUM_PARAM ; j ++ )
                paramNear[i][j] = -1;  
        }
        unsigned int const dataTarget[NUM_PROBE] = { 0xffff0000, 0xff00ff00, 0xffff0000, 0xff00ff00, 0x000000ff, 0x000000ff };
        //unsigned int const dataTarget[NUM_PROBE] = { 0xffff0000, 0xffffff00, 0xffff0000, 0xff00ffff, 0x000000ff, 0x000000ff };
        //unsigned int const dataTarget[NUM_PROBE] = { 0xffff0000, 0xff00ff00, 0xffff0000, 0x000000ff, 0x000000ff, 0x000000ff };
        //unsigned int const dataTarget[NUM_PROBE] = { 0xffff0000, 0xff00ff00, 0x00000000, 0xff00ff00, 0x00000000, 0x000000ff };
        int   param[NUM_PARAM];// = { 1,-1,-1,1,-1,-1 }; //en, ac, px/g/mx, ga, pd, pdm, zen, da
        for ( int en = 0, second = 0 ; en <= 2   ; en ++ ) 
        for ( int ac = 0, second = 0 ; ac <= 1   ; ac ++ ) 
        for ( int px = 0, second = 0 ; px <= 3   ; px ++ ) 
        for ( int ga = 0, second = 0 ; ga <= 255 ; ga += 127 ) 
        for ( int pd = 0, second = 0 ; pd <= 1   ; pd ++ ) 
        for ( int pm = 0, second = 0 ; pm <= 64  ; pm ++ ) 
        for ( int ze = 0, second = 0 ; ze <= 1   ; ze ++ ) 
        for ( int da = 0, second = 0 ; da <= 1   ; da ++ )
        {
            param[0] = en;
            param[1] = ac;
            param[2] = px;
            param[3] = ga;
            param[4] = pd;
            param[5] = pm;
            param[6] = ze;
            param[7] = da;

            clear( vaddr0, vaddr1 );
            blit( fdRGA, shareFd1, shareFd0, param );
            probe( vaddr0, dataCurr );


            int const df = diff( dataCurr, (int const*)dataTarget );
            if ( df < diffTmp ) {
                for ( int i = 0 ; i < NUM_CANDI ; i ++ ) {
                    if ( diffNear[i] == diffTmp ) {
                        memcpy( paramNear[i], param, sizeof(paramNear[i]) );
                        diffNear[i] = df;
                        break;
                    }
                }
                diffTmp = INT_MIN;
                for ( int i = 0 ; i < NUM_CANDI ; i ++ ) {
                    if ( diffTmp < diffNear[i] ) {
                        diffTmp = diffNear[i];
                    }
                }
            }
            

            /*
            //if ( memcmp( dataCurr, dataTarget, sizeof(dataCurr) ) == 0 ) {
            if ( dataCurr[0] == 0xffff0000 &&
                 ( dataCurr[2] & 0x80000000 ) &&
                 ( dataCurr[2] & 0x00800000 ) &&
                 
                 //dataCurr[4] == 0x000000ff &&
                 ( dataCurr[1] & 0x80000000 ) && 
                 ( dataCurr[1] & 0x00800000 ) && 
                 ( dataCurr[1] & 0x00008000 ) && 
                 ( dataCurr[3] & 0x80000000 ) && 
                 ( dataCurr[3] & 0x00008000 )
                ) {
            //if ( (dataCurr[2] & 0x00c00000 ) && (dataCurr[3] & 0xc0000000 ) &&
            //     ( (dataCurr[4] & 0xe0000000 )== 0 ) && ( dataCurr[4] & 0xc0 ) &&
            //     (dataCurr[2] & 0xc0000000 )
            //    ) {

                //if ( (int)dataTarget[2] == dataCurr[2] ) {
                printf( "after  %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x", dataCurr[0], dataCurr[1], dataCurr[2], dataCurr[3], dataCurr[4], dataCurr[5] );
                printf( ": en=%d ac=%d px=%d ga=%d pd=%d pm=%d ze=%d da=%d\n",
                        en, ac, px, ga, pd, pm, ze, da );
                //goto  out;
            }
            */

            /*
            if (second) {
                if ( memcmp( dataPrev, dataCurr, sizeof(dataPrev) ) ) {
                    printf( "\n en=%d px=%d ga=%d pd=%d pm=%d ze=%d da=%d\n",
                            en, px, ga, pd, pm, ze, da );
                    printf( "before %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n", dataPrev[0], dataPrev[1], dataPrev[2], dataPrev[3], dataPrev[4], dataPrev[5] );
                    printf( "after  %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n", dataCurr[0], dataCurr[1], dataCurr[2], dataCurr[3], dataCurr[4], dataCurr[5] );
                }
            } 
            memcpy( dataPrev, dataCurr, sizeof(dataPrev));
            second = 1;
            */


        
        }

        for ( int i = 0 ; i < NUM_CANDI ; i ++ ) {
            clear( vaddr0, vaddr1 );
            blit( fdRGA, shareFd1, shareFd0, paramNear[i] );
            printf("diff=%d", diffNear[i] );
            probe( vaddr0, "" );
            printParam( paramNear[i] );
        }

      out:;
    }

#endif
    

  ret:
    if ( errLine > 0 ) {
        printf("error: line=%d, %s\n", errLine, strerror(errno) );
    } else
        printf("OK\n");
    //...FB
    if ( shareFdFB > 0 )
        close(shareFdFB);
    if ( fdFB > 0 )
        close(fdFB);
    //...ion
    if (shareFd0) close(shareFd0);
    if (shareFd1) close(shareFd1);
    if (ion_hnd0) ion_free(ion_client,ion_hnd0);
    if (ion_hnd1) ion_free(ion_client,ion_hnd1);
    if (ion_client)
        ion_close( ion_client );
    //...rga
    if (fdRGA)
        close(fdRGA);

    //sleep( 2000 );
    return 0;
}


