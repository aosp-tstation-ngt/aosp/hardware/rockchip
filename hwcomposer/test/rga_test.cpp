#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/fb.h>
#include <stdlib.h>
#include <sys/mman.h>

#include  <stdio.h>
#include  <time.h>

#define ALOG(fmt,...) printf(fmt "\n",__VA_ARGS__)


#define RGA_BLIT_SYNC	0x5017
#define RGA_BLIT_ASYNC  0x5018
#define RGA_FLUSH       0x5019
#define RGA_GET_RESULT  0x501a
#define RGA_GET_VERSION 0x501b
#define RGA_REG_CTRL_LEN    0x8    /* 8  */
#define RGA_REG_CMD_LEN     0x1c   /* 28 */
#define RGA_CMD_BUF_SIZE    0x700  /* 16*28*4 */

typedef struct hwc_rect {
    int left;
    int top;
    int right;
    int bottom;
} hwc_rect_t;

/* RGA process mode enum */
//@@@@  (rga_req.render_mode )
enum
{    
    bitblt_mode               = 0x0,
    color_palette_mode        = 0x1,
    color_fill_mode           = 0x2,
    line_point_drawing_mode   = 0x3,
    blur_sharp_filter_mode    = 0x4,
    pre_scaling_mode          = 0x5,
    update_palette_table_mode = 0x6,
    update_patten_buff_mode   = 0x7,
};


enum
{
    rop_enable_mask          = 0x2,
    dither_enable_mask       = 0x8,
    fading_enable_mask       = 0x10,
    PD_enbale_mask           = 0x20,
};

enum
{
    yuv2rgb_mode0            = 0x0,     /* BT.601 MPEG */
    yuv2rgb_mode1            = 0x1,     /* BT.601 JPEG */
    yuv2rgb_mode2            = 0x2,     /* BT.709      */
};


/* RGA rotate mode */
enum 
{
    rotate_mode0             = 0x0,     /* no rotate */
    rotate_mode1             = 0x1,     /* rotate    */
    rotate_mode2             = 0x2,     /* x_mirror  */
    rotate_mode3             = 0x3,     /* y_mirror  */
};

enum
{
    color_palette_mode0      = 0x0,     /* 1K */
    color_palette_mode1      = 0x1,     /* 2K */
    color_palette_mode2      = 0x2,     /* 4K */
    color_palette_mode3      = 0x3,     /* 8K */
};

enum 
{
    BB_BYPASS   = 0x0,     /* no rotate */
    BB_ROTATE   = 0x1,     /* rotate    */
    BB_X_MIRROR = 0x2,     /* x_mirror  */
    BB_Y_MIRROR = 0x3      /* y_mirror  */
};

enum 
{
    nearby   = 0x0,     /* no rotate */
    bilinear = 0x1,     /* rotate    */
    bicubic  = 0x2,     /* x_mirror  */
};


/*
//          Alpha    Red     Green   Blue  
{  4, 32, {{32,24,   8, 0,  16, 8,  24,16 }}, GGL_RGBA },   // RK_FORMAT_RGBA_8888    
{  4, 24, {{ 0, 0,   8, 0,  16, 8,  24,16 }}, GGL_RGB  },   // RK_FORMAT_RGBX_8888    
{  3, 24, {{ 0, 0,   8, 0,  16, 8,  24,16 }}, GGL_RGB  },   // RK_FORMAT_RGB_888
{  4, 32, {{32,24,  24,16,  16, 8,   8, 0 }}, GGL_BGRA },   // RK_FORMAT_BGRA_8888
{  2, 16, {{ 0, 0,  16,11,  11, 5,   5, 0 }}, GGL_RGB  },   // RK_FORMAT_RGB_565        
{  2, 16, {{ 1, 0,  16,11,  11, 6,   6, 1 }}, GGL_RGBA },   // RK_FORMAT_RGBA_5551    
{  2, 16, {{ 4, 0,  16,12,  12, 8,   8, 4 }}, GGL_RGBA },   // RK_FORMAT_RGBA_4444
{  3, 24, {{ 0, 0,  24,16,  16, 8,   8, 0 }}, GGL_BGR  },   // RK_FORMAT_BGB_888

*/
typedef enum _Rga_SURF_FORMAT
{
	RK_FORMAT_RGBA_8888    = 0x0,
    RK_FORMAT_RGBX_8888    = 0x1,
    RK_FORMAT_RGB_888      = 0x2,
    RK_FORMAT_BGRA_8888    = 0x3,
    RK_FORMAT_RGB_565      = 0x4,
    RK_FORMAT_RGBA_5551    = 0x5,
    RK_FORMAT_RGBA_4444    = 0x6,
    RK_FORMAT_BGR_888      = 0x7,
    
    RK_FORMAT_YCbCr_422_SP = 0x8,    
    RK_FORMAT_YCbCr_422_P  = 0x9,    
    RK_FORMAT_YCbCr_420_SP = 0xa,    
    RK_FORMAT_YCbCr_420_P  = 0xb,

    RK_FORMAT_YCrCb_422_SP = 0xc,    
    RK_FORMAT_YCrCb_422_P  = 0xd,    
    RK_FORMAT_YCrCb_420_SP = 0xe,    
    RK_FORMAT_YCrCb_420_P  = 0xf,
    
    RK_FORMAT_BPP1         = 0x10,
    RK_FORMAT_BPP2         = 0x11,
    RK_FORMAT_BPP4         = 0x12,
    RK_FORMAT_BPP8         = 0x13,
    RK_FORMAT_YCbCr_420_SP_10B = 0x20,
    RK_FORMAT_YCrCb_420_SP_10B = 0x21,
    RK_FORMAT_UNKNOWN       = 0x100, 
}RgaSURF_FORMAT;
    
    
typedef struct rga_img_info_t
{
    unsigned int yrgb_addr;      /* yrgb    mem addr         */
    unsigned int uv_addr;        /* cb/cr   mem addr         */
    unsigned int v_addr;         /* cr      mem addr         */
    unsigned int format;         //definition by RK_FORMAT
    unsigned short act_w;
    unsigned short act_h;
    unsigned short x_offset;
    unsigned short y_offset;
    
    unsigned short vir_w;
    unsigned short vir_h;
    
    unsigned short endian_mode; //for BPP
    unsigned short alpha_swap;
}
rga_img_info_t;


typedef struct mdp_img_act
{
    unsigned short w;         // width
    unsigned short h;         // height
    short x_off;     // x offset for the vir
    short y_off;     // y offset for the vir
}
mdp_img_act;



typedef struct RANGE
{
    unsigned short min;
    unsigned short max;
}
RANGE;

typedef struct POINT
{
    unsigned short x;
    unsigned short y;
}
POINT;

typedef struct RECT
{
    unsigned short xmin;
    unsigned short xmax; // width - 1
    unsigned short ymin; 
    unsigned short ymax; // height - 1 
} RECT;

typedef struct RGB
{
    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char res;
}RGB;


typedef struct MMU
{
    unsigned char mmu_en;
    unsigned int base_addr;
    unsigned int mmu_flag;     /* [0] mmu enable [1] src_flush [2] dst_flush [3] CMD_flush [4~5] page size*/
} MMU;




typedef struct COLOR_FILL
{
    short gr_x_a;
    short gr_y_a;
    short gr_x_b;
    short gr_y_b;
    short gr_x_g;
    short gr_y_g;
    short gr_x_r;
    short gr_y_r;

    //u8  cp_gr_saturation;
}
COLOR_FILL;

typedef struct FADING
{
    unsigned char b;
    unsigned char g;
    unsigned char r;
    unsigned char res;
}
FADING;


typedef struct line_draw_t
{
    POINT start_point;              /* LineDraw_start_point                */
    POINT end_point;                /* LineDraw_end_point                  */
    unsigned int   color;               /* LineDraw_color                      */
    unsigned int   flag;                /* (enum) LineDrawing mode sel         */
    unsigned int   line_width;          /* range 1~16 */
}
line_draw_t;



 struct rga_req { 
    unsigned char render_mode;            /* (enum) process mode sel */
    
    rga_img_info_t src;                   /* src image info */
    rga_img_info_t dst;                   /* dst image info */
    rga_img_info_t pat;             /* patten image info */

    unsigned int rop_mask_addr;         /* rop4 mask addr */
    unsigned int LUT_addr;              /* LUT addr */
    RECT clip;                      /* dst clip window default value is dst_vir */
                                    /* value from [0, w-1] / [0, h-1]*/
        
    int sina;                   /* dst angle  default value 0  16.16 scan from table */
    int cosa;                   /* dst angle  default value 0  16.16 scan from table */        

    unsigned short alpha_rop_flag;        /* alpha rop process flag           */
                                    /* ([0] = 1 alpha_rop_enable)       */
                                    /* ([1] = 1 rop enable)             */                                                                                                                
                                    /* ([2] = 1 fading_enable)          */
                                    /* ([3] = 1 PD_enable)              */
                                    /* ([4] = 1 alpha cal_mode_sel)     */
                                    /* ([5] = 1 dither_enable)          */
                                    /* ([6] = 1 gradient fill mode sel) */
                                    /* ([7] = 1 AA_enable)              */

    unsigned char  scale_mode;            /* 0 nearst / 1 bilnear / 2 bicubic */                             
                            
    unsigned int color_key_max;         /* color key max */
    unsigned int color_key_min;         /* color key min */     

    unsigned int fg_color;              /* foreground color */
    unsigned int bg_color;              /* background color */

    COLOR_FILL gr_color;            /* color fill use gradient */
    
    line_draw_t line_draw_info;
    
    FADING fading;
                              
    unsigned char PD_mode;                /* porter duff alpha mode sel */
    
    unsigned char alpha_global_value;     /* global alpha value */
     
    unsigned short rop_code;              /* rop2/3/4 code  scan from rop code table*/
    
    unsigned char bsfilter_flag;          /* [2] 0 blur 1 sharp / [1:0] filter_type*/
    
    unsigned char palette_mode;           /* (enum) color palatte  0/1bpp, 1/2bpp 2/4bpp 3/8bpp*/

    unsigned char yuv2rgb_mode;           /* (enum) BT.601 MPEG / BT.601 JPEG / BT.709  */ 
    
    unsigned char endian_mode;            /* 0/big endian 1/little endian*/

    unsigned char rotate_mode;            /* (enum) rotate mode  */
                                    /* 0x0,     no rotate  */
                                    /* 0x1,     rotate     */
                                    /* 0x2,     x_mirror   */
                                    /* 0x3,     y_mirror   */

    unsigned char color_fill_mode;        /* 0 solid color / 1 patten color */
                                    
    MMU mmu_info;                   /* mmu information */

    unsigned char  alpha_rop_mode;        /* ([0~1] alpha mode)       */
                                    /* ([2~3] rop   mode)       */
                                    /* ([4]   zero  mode en)    */
                                    /* ([5]   dst   alpha mode) */

    unsigned char  src_trans_mode;

    unsigned char CMD_fin_int_enable;                        

    /* completion is reported through a callback */
	void (*complete)(int retval);
};



int
RGA_set_src_act_info(
		struct rga_req *req,
		unsigned int   width,       /* act width  */
		unsigned int   height,      /* act height */
		unsigned int   x_off,       /* x_off      */
		unsigned int   y_off        /* y_off      */
		);

int
RGA_set_src_vir_info(
		struct rga_req *req,
		unsigned int   yrgb_addr,       /* yrgb_addr  */
		unsigned int   uv_addr,         /* uv_addr    */
		unsigned int   v_addr,          /* v_addr     */
		unsigned int   vir_w,           /* vir width  */
		unsigned int   vir_h,           /* vir height */
		unsigned char  format,          /* format     */
		unsigned char  a_swap_en        /* only for 32bit RGB888 format */
		);

int
RGA_set_dst_act_info(
		struct rga_req *req,
		unsigned int   width,       /* act width  */
		unsigned int   height,      /* act height */
		unsigned int   x_off,       /* x_off      */
		unsigned int   y_off        /* y_off      */
		);

int
RGA_set_dst_vir_info(
		struct rga_req *msg,
		unsigned int   yrgb_addr,   /* yrgb_addr   */
		unsigned int   uv_addr,     /* uv_addr     */
		unsigned int   v_addr,      /* v_addr      */
		unsigned int   vir_w,       /* vir width   */
		unsigned int   vir_h,       /* vir height  */
		RECT           *clip,        /* clip window */
		unsigned char  format,      /* format      */
		unsigned char  a_swap_en
		);

int 
RGA_set_pat_info(
    struct rga_req *msg,
    unsigned int width,
    unsigned int height,
    unsigned int x_off,
    unsigned int y_off,
    unsigned int pat_format    
    );

int
RGA_set_rop_mask_info(
		struct rga_req *msg,
		unsigned int rop_mask_addr,
		unsigned int rop_mask_endian_mode
		);
   
int RGA_set_alpha_en_info(
		struct rga_req *msg,
		unsigned int  alpha_cal_mode,    /* 0:alpha' = alpha + (alpha>>7) | alpha' = alpha */
		unsigned int  alpha_mode,        /* 0 global alpha / 1 per pixel alpha / 2 mix mode */
		unsigned int  global_a_value,
		unsigned int  PD_en,             /* porter duff alpha mode en */ 
		unsigned int  PD_mode, 
		unsigned int  dst_alpha_en );    /* use dst alpha  */

int
RGA_set_rop_en_info(
		struct rga_req *msg,
		unsigned int ROP_mode,
		unsigned int ROP_code,
		unsigned int color_mode,
		unsigned int solid_color
		);

 
int
RGA_set_fading_en_info(
		struct rga_req *msg,
		unsigned char r,
		unsigned char g,
		unsigned char b
		);

int
RGA_set_src_trans_mode_info(
		struct rga_req *msg,
		unsigned char trans_mode,
		unsigned char a_en,
		unsigned char b_en,
		unsigned char g_en,
		unsigned char r_en,
		unsigned char color_key_min,
		unsigned char color_key_max,
		unsigned char zero_mode_en
		);


int
RGA_set_bitblt_mode(
		struct rga_req *msg,
		unsigned char scale_mode,    // 0/near  1/bilnear  2/bicubic  
		unsigned char rotate_mode,   // 0/copy 1/rotate_scale 2/x_mirror 3/y_mirror 
		unsigned int  angle,         // rotate angle     
		unsigned int  dither_en,     // dither en flag   
		unsigned int  AA_en,         // AA flag          
		unsigned int  yuv2rgb_mode
		);


int
RGA_set_color_palette_mode(
		struct rga_req *msg,
		unsigned char  palette_mode,        /* 1bpp/2bpp/4bpp/8bpp */
		unsigned char  endian_mode,         /* src endian mode sel */
		unsigned int  bpp1_0_color,         /* BPP1 = 0 */
		unsigned int  bpp1_1_color          /* BPP1 = 1 */
		);


int
RGA_set_color_fill_mode(
		struct rga_req *msg,
		COLOR_FILL  *gr_color,                    /* gradient color part         */
		unsigned char  gr_satur_mode,            /* saturation mode             */
    	unsigned char  cf_mode,                  /* patten fill or solid fill   */
		unsigned int color,                      /* solid color                 */
		unsigned short pat_width,                /* pattern width               */
		unsigned short pat_height,               /* pattern height              */   
		unsigned char pat_x_off,                 /* pattern x offset            */
		unsigned char pat_y_off,                 /* pattern y offset            */
		unsigned char aa_en                      /* alpha en                    */
		);


int
RGA_set_line_point_drawing_mode(
		struct rga_req *msg,
		POINT sp,                     /* start point              */
		POINT ep,                     /* end   point              */
		unsigned int color,           /* line point drawing color */
		unsigned int line_width,      /* line width               */
		unsigned char AA_en,          /* AA en                    */
		unsigned char last_point_en   /* last point en            */
		);


int
RGA_set_blur_sharp_filter_mode(
		struct rga_req *msg,
		unsigned char filter_mode,   /* blur/sharpness   */
		unsigned char filter_type,   /* filter intensity */
		unsigned char dither_en      /* dither_en flag   */
		);

int
RGA_set_pre_scaling_mode(
		struct rga_req *msg,
		unsigned char dither_en
		);

int
RGA_update_palette_table_mode(
		struct rga_req *msg,
		unsigned int LUT_addr,      /* LUT table addr      */
		unsigned int palette_mode   /* 1bpp/2bpp/4bpp/8bpp */
		);

int
RGA_set_update_patten_buff_mode(
		struct rga_req *msg,
		unsigned int pat_addr, /* patten addr    */
		unsigned int w,        /* patten width   */
		unsigned int h,        /* patten height  */
		unsigned int format    /* patten format  */
		);

int
RGA_set_mmu_info(
		struct rga_req *msg,
		unsigned char  mmu_en,
		unsigned char  src_flush,
		unsigned char  dst_flush,
		unsigned char  cmd_flush,
		unsigned int base_addr,
		unsigned char  page_size
		);

void rga_set_fds_offsets(
        struct rga_req *rga_request,
        unsigned short src_fd,
        unsigned short dst_fd,
        unsigned int src_offset, 
        unsigned int dst_offset);

int
RGA_set_src_fence_flag(
    struct rga_req *msg,
    int acq_fence,
    int src_flag
);


int
RGA_set_dst_fence_flag(
    struct rga_req *msg,
    int dst_flag
);

int
RGA_get_dst_fence(
    struct rga_req *msg
);

//#define PAGE_SIZE 4096
inline size_t round_up_to_page_size(size_t x)
{
	return (x + (PAGE_SIZE - 1)) & ~(PAGE_SIZE - 1);
}

int
RGA_set_dst_vir_info(
		struct rga_req *msg,
		unsigned int   yrgb_addr,   /* yrgb_addr   */
		unsigned int   uv_addr,     /* uv_addr     */
		unsigned int   v_addr,      /* v_addr      */
		unsigned int   vir_w,       /* vir width   */
		unsigned int   vir_h,       /* vir height  */
		RECT           *clip,        /* clip window */
		unsigned char  format,      /* format      */
		unsigned char  a_swap_en
		)
{
    msg->dst.yrgb_addr = yrgb_addr;
    msg->dst.uv_addr  = uv_addr;
    msg->dst.v_addr   = v_addr;
    msg->dst.vir_w = vir_w;
    msg->dst.vir_h = vir_h;
    msg->dst.format = format;

    msg->clip.xmin = clip->xmin;
    msg->clip.xmax = clip->xmax;
    msg->clip.ymin = clip->ymin;
    msg->clip.ymax = clip->ymax;
    
    msg->dst.alpha_swap |= (a_swap_en & 1);
    
    return 1;
}

void rga_set_fds_offsets(struct rga_req *rga_request,
			 uint16_t src_fd, uint16_t dst_fd,
			 uint32_t src_offset, uint32_t dst_offset)
{
       rga_request->line_draw_info.color = src_fd | (dst_fd << 16);
       rga_request->line_draw_info.flag = src_offset;
       rga_request->line_draw_info.line_width = dst_offset;
}


int
RGA_set_mmu_info(
		struct rga_req *msg,
		unsigned char  mmu_en,
		unsigned char  src_flush,
		unsigned char  dst_flush,
		unsigned char  cmd_flush,
		unsigned int base_addr,
		unsigned char  page_size
		)
{
    msg->mmu_info.mmu_en    = mmu_en;
    msg->mmu_info.base_addr = base_addr;
    msg->mmu_info.mmu_flag  = ((page_size & 0x3) << 4) |
                              ((cmd_flush & 0x1) << 3) |
                              ((dst_flush & 0x1) << 2) | 
                              ((src_flush & 0x1) << 1) | mmu_en;
    return 1;
}

int
RGA_set_color_fill_mode(
		struct rga_req *msg,
		COLOR_FILL  *gr_color,              /* gradient color part         */
		unsigned char  gr_satur_mode,            /* saturation mode             */
    	unsigned char  cf_mode,                  /* patten fill or solid fill   */
		unsigned int color,                    /* solid color                 */
		unsigned short pat_width,                /* pattern width               */
		unsigned short pat_height,               /* pattern height              */   
		unsigned char pat_x_off,                 /* pattern x offset            */
		unsigned char pat_y_off,                 /* pattern y offset            */
		unsigned char aa_en                      /* alpha en                    */
		)
{
    msg->render_mode = color_fill_mode;

    msg->gr_color.gr_x_a = ((int)(gr_color->gr_x_a * 256.0))& 0xffff;
    msg->gr_color.gr_x_b = ((int)(gr_color->gr_x_b * 256.0))& 0xffff;
    msg->gr_color.gr_x_g = ((int)(gr_color->gr_x_g * 256.0))& 0xffff;
    msg->gr_color.gr_x_r = ((int)(gr_color->gr_x_r * 256.0))& 0xffff;

    msg->gr_color.gr_y_a = ((int)(gr_color->gr_y_a * 256.0))& 0xffff;
    msg->gr_color.gr_y_b = ((int)(gr_color->gr_y_b * 256.0))& 0xffff;
    msg->gr_color.gr_y_g = ((int)(gr_color->gr_y_g * 256.0))& 0xffff;
    msg->gr_color.gr_y_r = ((int)(gr_color->gr_y_r * 256.0))& 0xffff;

    msg->color_fill_mode = cf_mode;
    
    msg->pat.act_w = pat_width;
    msg->pat.act_h = pat_height;

    msg->pat.x_offset = pat_x_off;
    msg->pat.y_offset = pat_y_off;

    msg->fg_color = color;

    msg->alpha_rop_flag |= ((gr_satur_mode & 1) << 6);
    
    if(aa_en)
    {
    	msg->alpha_rop_flag |= 0x1;
    	msg->alpha_rop_mode  = 1;
    }
    return 1;
}

int
RGA_set_dst_act_info(
		struct rga_req *req,
		unsigned int   width,       /* act width  */
		unsigned int   height,      /* act height */
		unsigned int   x_off,       /* x_off      */
		unsigned int   y_off        /* y_off      */
		)
{
    req->dst.act_w = width;
    req->dst.act_h = height;
    req->dst.x_offset = x_off;
    req->dst.y_offset = y_off;
    
    return 1;
}


//void (*complete)(int retval);
void complete_fnc(int retval)
{
  ALOG("complete_fnc(%d)", retval);
}

void hwcClear(
              int     fdRGA,
              unsigned int Color,
              hwc_rect_t const& DstRect,
              int           DstFD,
              void*       DstBase,
              int         DstOffset //in bytes
              )
{
  void *     dstLogical;
  unsigned int      dstFd = ~0;//@@@@, dstBase = ~0;
  unsigned int      dstStride;
  unsigned int      dstWidth;
  unsigned int      dstHeight;
  RgaSURF_FORMAT  dstFormat;

  struct rga_req  Rga_Request;
  RECT clip;
  unsigned int      Xoffset;
  unsigned int      Yoffset;
  unsigned int      WidthAct;
  unsigned int      HeightAct;
  COLOR_FILL FillColor ;

  memset(&FillColor , 0x0, sizeof(COLOR_FILL));
  memset(&Rga_Request, 0x0, sizeof(Rga_Request));
  Rga_Request.complete = &complete_fnc;
  Rga_Request.CMD_fin_int_enable = 1;

  dstWidth = DstRect.right - DstRect.left;
  dstHeight = DstRect.bottom - DstRect.top;
  dstStride = 1024;
  dstFormat = RK_FORMAT_RGBA_8888;
  /* <<< End surface information. */

  clip.xmin = 0;
  clip.xmax = dstWidth - 1;
  clip.ymin = 0;
  clip.ymax = dstHeight - 1;

  dstFd = DstFD;
  //NG RGA_set_dst_vir_info(&Rga_Request, dstFd, (uintptr_t)DstBase, 0, dstStride, dstHeight, &clip, dstFormat, 0);
  //RGA_set_dst_vir_info(&Rga_Request, dstFd, (uintptr_t)DstBase, 0, dstStride, 600*3, &clip, dstFormat, 0);  //OK
  //NG RGA_set_dst_vir_info(&Rga_Request, dstFd, (uintptr_t)DstBase+DstOffset, 0, dstStride, 600, &clip, dstFormat, 0);

  //RGA_set_dst_vir_info(&Rga_Request, dstFd, (uintptr_t)DstBase, 0, dstStride, DstOffset/(1024*4)+600, &clip, dstFormat, 0);//OK
  //rga_set_fds_offsets(&Rga_Request, 0, 0, 0, 0); // already cleared by memset(,0,)

  RGA_set_dst_vir_info(&Rga_Request, dstFd, (uintptr_t)DstBase, 0, dstWidth, DstOffset/(1024*4)+600, &clip, dstFormat, 0);//OK

  // for test
  //RGA_set_dst_vir_info(&Rga_Request, dstFd, (uintptr_t)DstBase, 0, dstStride, dstHeight, &clip, dstFormat, 0);
  //RGA_set_dst_vir_info(&Rga_Request, dstFd, (uintptr_t)DstBase, 0, dstStride, DstOffset/(1024*4)+600, &clip, dstFormat, 0);//OK
  //rga_set_fds_offsets(&Rga_Request, 0, dstFd, 0, DstOffset);

  RGA_set_color_fill_mode(&Rga_Request, &FillColor, 0, 0, Color, 0, 0, 0, 0, 0);

  Xoffset   = DstRect.left;
  Yoffset   = DstRect.top + DstOffset/(4*1024);
  WidthAct  = DstRect.right  - DstRect.left ;
  HeightAct = DstRect.bottom - DstRect.top ;

  RGA_set_mmu_info(&Rga_Request, 1, 0, 0, 0, 0, 2);
  Rga_Request.mmu_info.mmu_flag |= (1 << 31) | (1 << 10) | (0 << 8);

  RGA_set_dst_act_info(&Rga_Request, WidthAct, HeightAct, Xoffset, Yoffset);

  if (ioctl(fdRGA, RGA_BLIT_ASYNC, &Rga_Request) != 0) {
      ALOG("%s(%d):  RGA_BLIT_ASYNC Failed", __FUNCTION__, __LINE__);
    }
  if ( ioctl(fdRGA, RGA_FLUSH, NULL) != 0 ) {
      ALOG("%s(%d):  RGA_BLIT_ASYNC Failed", __FUNCTION__, __LINE__);
  }
}

int main(int argc, const char*argv[] )
{
  int       errLine = -1;
  int		    fdFB = 0;
  int const sizeFB = 600*1024*4;  // rgba

  int       index;

  int*      pBuff; 
  size_t    sizeFBALL;
	void *    vaddr;
  //TIME_DECLARE(t);


  // index <-- cmd line
  if ( argc != 2 ) {
    printf("usage : %s <index(0..2)>", argv[0] );
		errLine = __LINE__; goto ret;
  }
  index = atoi( argv[1] );
  if ( index < 0 || index > 2 ) {
		errLine = __LINE__; goto ret;
  }


  fdFB = open("/dev/graphics/fb0", O_RDWR, 0);
	if ( fdFB <= 0 ) {
		errLine = __LINE__; goto ret;
	}

  sizeFBALL = round_up_to_page_size(sizeFB*3);
	vaddr = mmap(0, sizeFBALL, PROT_READ | PROT_WRITE, MAP_SHARED, fdFB, 0);
	if (vaddr == MAP_FAILED) 	{
		errLine = __LINE__; goto ret;
	}

  
  // clear fb(partially)
#if 1
  {
    hwc_rect_t rectDst = { 0, 0, 512, 300 };
    int const offset = sizeFB * index;
    int fdRGA = open("/dev/rga", O_RDWR, 0);
    int shared_fd;
		if ( ioctl(fdFB, /*FBIOGET_DMABUF*/0x5003, &shared_fd) != 0 ) {
      errLine = __LINE__; goto ret;
    }
    if ( fdRGA <= 0 ) {
      errLine = __LINE__; goto ret;
    }
    hwcClear( fdRGA, 0xffff0000, rectDst, shared_fd, vaddr, offset );
    if (ioctl(fdRGA, RGA_FLUSH, NULL) != 0) {
      errLine = __LINE__; goto ret;
    }

  }
#else
  pBuff = (int*)((intptr_t)vaddr + sizeFB * index ); 
  for ( int cnt = 600*1024 / 4 ; -- cnt >= 0 ; ) {
    *pBuff ++ = 0xffff0000;
  }
#endif


  
 ret:
  if ( errLine > 0 ) {
    printf("error: line=%d, %s\n", errLine, strerror(errno) );
  } else
    printf("OK\n");
  if ( fdFB > 0 )
    close(fdFB);

  sleep( 2000 );
  return 0;
}

