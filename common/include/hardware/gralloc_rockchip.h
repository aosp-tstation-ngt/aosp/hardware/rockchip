/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef ANDROID_GRALLOC_ROCKCHIP_INTERFACE_H
#define ANDROID_GRALLOC_ROCKCHIP_INTERFACE_H

#include <system/window.h>
#include <system/graphics.h>
#include <hardware/hardware.h>

#include <stdint.h>
#include <sys/cdefs.h>
#include <sys/types.h>

#include <cutils/native_handle.h>

#include <hardware/hardware.h>
#include <hardware/fb.h>

__BEGIN_DECLS

/**
 * Rockchip specific definitions.
 */

enum {
    GRALLOC_USAGE_ROT_MASK              = 0x0F000000,   // .KP : 目前实际上不用了, 但代码上有依赖.
    /**
     * mask for rk_nv12_10_color_space_field.
     */
    GRALLOC_USAGE_RK_COLOR_SPACES_MASK  = 0x0F000000,

    GRALLOC_USAGE_TO_USE_SINGLE_BUFFER  = 0x08000000,
    /* would like to use a fbdc(afbc) format. */
    GRALLOC_USAGE_TO_USE_FBDC_FMT       = 0x04000000,
};

/**
 * perform operation commands for rk gralloc.
 * Helpers for using the non-type-safe perform() extension functions. Use
 * these helpers instead of calling perform() directly in your application.
 */
enum {
  /****************Implement****************/
  GRALLOC_MODULE_PERFORM_GET_HADNLE_PRIME_FD       = 0x08100002,
  GRALLOC_MODULE_PERFORM_GET_HADNLE_ATTRIBUTES     = 0x08100004,
  GRALLOC_MODULE_PERFORM_GET_INTERNAL_FORMAT       = 0x08100006,
  GRALLOC_MODULE_PERFORM_GET_HADNLE_WIDTH          = 0x08100008,
  GRALLOC_MODULE_PERFORM_GET_HADNLE_HEIGHT         = 0x0810000A,
  GRALLOC_MODULE_PERFORM_GET_HADNLE_STRIDE         = 0x0810000C,
  GRALLOC_MODULE_PERFORM_GET_HADNLE_BYTE_STRIDE    = 0x0810000E,
  GRALLOC_MODULE_PERFORM_GET_HADNLE_FORMAT         = 0x08100010,
  GRALLOC_MODULE_PERFORM_GET_HADNLE_SIZE           = 0x08100012,

  /* perform(const struct gralloc_module_t *mod,
   *     int op,
   *     buffer_handle_t buffer,
   *     int *usage);
   */
  GRALLOC_MODULE_PERFORM_GET_USAGE = 0x0feeff03,


  /****************Not Implement****************/
  GRALLOC_MODULE_PERFORM_GET_DRM_FD                = 0x08000002,
  /* perform(const struct gralloc_module_t *mod,
   *	   int op,
   *	   int drm_fd,
   *	   buffer_handle_t buffer,
   *	   struct hwc_drm_bo *bo);
   */
  GRALLOC_MODULE_PERFORM_DRM_IMPORT = 0xffeeff00,

  /* perform(const struct gralloc_module_t *mod,
   *	   int op,
   *	   buffer_handle_t buffer,
   *	   void (*free_callback)(void *),
   *	   void *priv);
   */
  GRALLOC_MODULE_PERFORM_SET_IMPORTER_PRIVATE = 0xffeeff01,

  /* perform(const struct gralloc_module_t *mod,
   *	   int op,
   *	   buffer_handle_t buffer,
   *	   void (*free_callback)(void *),
   *	   void **priv);
   */
  GRALLOC_MODULE_PERFORM_GET_IMPORTER_PRIVATE = 0xffeeff02,
};

typedef int rk_nv12_10_color_space_t;
#define RK_NV12_10_BT709                ( (rk_nv12_10_color_space_t)(0) )
#define RK_NV12_10_COLOR_SPACE_DEFAULT  RK_NV12_10_BT709
#define RK_NV12_10_BT2020               ( (rk_nv12_10_color_space_t)(1) )
#define RK_NV12_10_HDR_10               ( (rk_nv12_10_color_space_t)(2) )
#define RK_NV12_10_HDR_HLG              ( (rk_nv12_10_color_space_t)(3) )
#define RK_NV12_10_HDR_DOLBY            ( (rk_nv12_10_color_space_t)(4) )

inline rk_nv12_10_color_space_t get_rk_color_space_from_usage(int usage)
{
    return (usage & GRALLOC_USAGE_RK_COLOR_SPACES_MASK) >> 24;
}

inline void set_rk_color_space_into_usage(rk_nv12_10_color_space_t color_space,
                                          int* usage)
{
    int usage_input = *usage;

    *usage = (color_space << 24) | ( usage_input & ~GRALLOC_USAGE_RK_COLOR_SPACES_MASK);
}
/*****************************************************************************/
__END_DECLS

#endif  // ANDROID_GRALLOC_INTERFACE_H
